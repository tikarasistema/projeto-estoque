<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tikara Sistemas | <?php echo $_SESSION['StockPower']['name']; ?></title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/AdminLTE.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

</head>
<section class="content container-fluid">
    <div class="panel panel-info">
        <div class="panel-heading text-center"><h4><strong>RELATÓRIO</strong> - VENDAS</h4></div>
        <div class="panel-body">
            <div class="table-responsive">
                <div>
                    <input type="button" class="btn btn-success pull-right" value="Imprimir" onclick="printpage()"/>
                </div>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Data</th>
                            <th>Pagamento</th>
                            <th>Venda</th>
                            <th>Total Bruto</th>
                            <th>Desc.%</th>
                            <th>Desc.R$</th>
                            <th>Total Líquido</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($list)) { ?>
                            <?php foreach ($list as $v) { ?>
                                <tr>
                                    <td><?php echo $v['id']; ?></td>
                                    <td><?php echo $v['nome']; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($v['data'])); ?></td>
                            <td>
                                <?php if ($v['situacao'] == AGUARDANDO_PAGAMENTO) { ?>
                                    <?php echo 'Aguardando Pgto'; ?>
                                <?php } elseif ($v['situacao'] == PAGO) { ?>
                                    <?php echo 'Pago'; ?>
                                <?php } elseif ($v['situacao'] == CANCELADO) { ?>
                                    <?php echo 'Cancelado'; ?>
                                <?php } ?>   
                            </td>
                            <td>
                                <?php
                                if ($v['status_venda'] == STATUS_VENDA_FECHADA) {
                                    echo "<div class='situacao_inativo'>Fechada</div>";
                                } else {
                                    echo "<div class='situacao_ativo'>Aberta</div>";
                                } ;?> 
                            </td>
                                    <td><?php echo $v['total_preco']; ?></td>
                                    <td><?php echo $v['desconto']; ?></td>
                                    <td><?php echo $v['vldesconto']; ?></td>
                                    <td><?php echo $v['totalfinal']; ?></td>
                                <?php } ?>
                                <?php } else { ?><tr>
                                <td  class="text-center" colspan="12">Nenhuma informação a ser exibida</td>
                            </tr> <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer"> <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="" target="_blank">Tikara Sistemas | Marcelo Miyashita</a>.</strong> Todos os Direitos Reservados.</div>
    </div>
</section>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/relatorios.js"></script>
