<?php

$mostratelas = implode(",", $telas[0]);

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$resultado = substr($url_atual,0,-2);
$pattern = ',' . $resultado . ',';
;?>

<?php if (preg_match($pattern, $mostratelas)) { ?>
 
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Clientes <i class="fa fa-users"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
    <?php if(!empty($msg)) {echo $msg;} ?>
    <form method="POST" name="registration" action="<?php echo BASE_URL ?>clientes/edit/<?php echo $info['idcliente'] ?>">
        <div class="box">
            <div class="box-header">
                <div class="box-title"> Editar dados do Cliente - #<?php echo $info['idcliente']; ?></div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $info['nome'] ?>">
<!--                            <span class='msg-erro msg-nome'></span>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Sobrenome</label>
                            <input type="text" name="sobrenome" id="sobrenome" class="form-control" value="<?php echo $info['sobrenome'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*E-mail</label>
                            <input type="email" name="email" id="email" class="form-control" value="<?php echo $info['email'] ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Telefone</label>
                            <input type="text" name="telefone" id="telefone" class="form-control" value="<?php echo $info['telefone'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*CPF</label>
                            <input type="text" name="cpf" id="cpf" maxlength="18" class="form-control" onBlur="validaFormato(this);" onkeypress="return (apenasNumeros(event))" value="<?php echo $info['cpf'] ?>">
                        <div id="divResultado"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*CEP</label>
                            <input type="text" name="cep" id="cep" maxlength="8" class="form-control"  value="<?php echo $info['cep'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-6">            
                        <div class="form-group">
                            <label>*Logradouro</label>
                            <input type="text" name="logradouro" id="logradouro" class="form-control" value="<?php echo $info['logradouro'] ?>"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Número</label>
                            <input type="text" name="numero" id="numero" class="form-control" value="<?php echo $info['numero'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Complemento</label>
                            <input type="text" name="complemento" id="complemento" class="form-control" value="<?php echo $info['complemento'] ?>">   
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>*Bairro</label>
                            <input type="text" name="bairro" id="bairro" class="form-control" value="<?php echo $info['bairro'] ?>"> 
                        </div>
                    </div>
                </div>

                <div class="row">   
                    <div class="col-md-6">            
                        <div class="form-group">
                            <label>*Estado</label>
                            <input type="text" name="estado" id="estado" class="form-control" value="<?php echo $info['estado'] ?>"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Cidade</label>
                            <input type="text" name="cidade" id="cidade" class="form-control" value="<?php echo $info['cidade'] ?>"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <div class="checkbox">
                                <label for="situacao">
                                    <input type="checkbox" name="situacao" id="situacao"
                                           <?php if ($info['situacao'] == 1) { ?> checked<?php } ?>
                                           > Cliente ativo
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-tools">
                    <input type="submit" id="success-btn" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/clientes.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#cep").on("change", function () {
            if (this.value) {
                $.ajax({
                    url: 'http://api.postmon.com.br/v1/cep/' + this.value,
                    dateType: "json",
                    crossDomain: true,
                    statusCode: {
                        200: function (data) {
                            //console.log(data);

                            $("#cep").addClass("is-valid");
                            $("#logradouro").val(data.logradouro);
                            //$("#bairro").val(data.bairro);
                            $("#cidade").val(data.cidade);
                            $("#estado").val(data.estado);
                        },
                        400: function (msg) {
                            console.log(msg); //Request error
                        },
                        404: function (msg) {
                            console.log(msg); //Cep inválido
                        }
                    }
                })
            }
        });
    });
</script>
