<?php 

//$mostratelas = $telas[0][0];
$mostratelas = isset($telas[0][0]) ? ($telas[0][0]) : 0;

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Produtos <i class="fa fa-barcode"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
<?php if(!empty($msg)) {echo $msg;} ?>
    <form id="frmProduto" method="POST" name="registration" action="<?php echo BASE_URL ?>produtos/add" enctype="multipart/form-data">

        <div class="box">
            <div class="box-header">
                <div class="box-title"> Adicionar novo Produto</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Preço</label>
                            <input type="text" name="preco" id="preco" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Quantidade</label>
                            <input type="qtde" name="qtde" id="qtde" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Quantidade mínima</label>
                            <input type="text" name="qtde_min" id="qtde_min" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>*Categorias</label>
                            <select name="categoria" id="categoria" class="form-control">
                                <?php foreach ($categoria_list as $categoria) { ?>
                                    <option value="<?php echo $categoria['categoria']; ?>">
                                        <?php echo $categoria['categoria']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>*Fornecedores</label>
                            <select name="fornecedor" id="fornecedor" class="form-control">
                                <?php foreach ($fornecedor_list as $fornecedor) { ?>
                                    <option value="<?php echo $fornecedor['nome']; ?>">
                                        <?php echo $fornecedor['nome']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">            
                        <div class="form-group">
                            <label>*Imagem</label>
                            <input type="file" name="imagem" id="imagem" class="form-control"> 
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <select name="situacao" id="situacao" class="form-control">
                                <option value="">Selecione...</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="box-tools">
                    <input id="btnAdicionar" name="adicionarProduto" type="submit" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/produtos.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

<script>
    function validarFormVazio(formulario) {
        dados = $('#' + formulario).serialize();
        d = dados.split('&');
        vazios = 0;
        for (i = 0; i < d.length; i++) {
            controles = d[i].split("=");
            if (controles[1] == "A" || controles[1] == "") {
                vazios++;
            }
        }
        return vazios;
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdicionar').click(function () {

            vazios = validarFormVazio('frmProduto');

            if (vazios > 0) {
                alertify.alert("Preencha os campos.");
                return false;
            }
        });
    });
</script>