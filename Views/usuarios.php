<?php

//$mostratelas = $telas[0][0];
$mostratelas = isset($telas[0][0]) ? ($telas[0][0]) : 0;


$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Usuários <i class="glyphicon glyphicon-user"></i>
    </h1>
</section>
<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <div class="box-title"> Listagem de Usuários</div>
            <div class="box-tools">
                <a href="<?php echo BASE_URL ?>usuarios/add" class="btn btn-success"> + Adicionar</a>
            </div> 
        </div>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Situação</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list as $usuario) { ?>
                        <tr>
                            <td><?php echo $usuario['id']; ?></td>
                             <td>
                                <?php
                                if ($usuario['situacao'] == SITUACAO_INATIVO) {
                                    echo "<div class='situacao_inativo'>Inativo</div>";
                                } else {
                                    echo "<div class='situacao_ativo'>Ativo</div>";
                                } ;?> 
                            </td>
                            <td><?php echo $usuario['name']; ?></td>
                            <td><?php echo $usuario['email']; ?></td>
                            <td>
                                <a class="btn btn-success btn-xs" href="<?php echo BASE_URL . 'usuarios/edit/' . $usuario['id'] ?>">Editar</a>
                                <a class="open-excluirRegistro btn btn-danger btn-xs" data-delete-registro="<?php echo $usuario['id']; ?>"  href="" onClick="ShowModal(this)" data-toggle="modal" data-target="#delete-modal">Excluir</a>
                            </td>
                            <?php } ?>
                        </tr>
                    </tbody>
            </table>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalLabel">Excluir Registro</h4>
            </div>
            <div class="modal-body">
                Deseja realmente excluir este registro?
            </div>
            <div class="modal-footer">
                <a id="acaoRemoverRegistro" class="btn btn-danger" href="#">Sim</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
            </div>
        </div>
    </div>
</div>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/usuarios.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>
