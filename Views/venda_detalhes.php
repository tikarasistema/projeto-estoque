<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Vendas <i class="glyphicon glyphicon-shopping-cart"></i>
    </h1>
</section>

<section class="content container-fluid"> 
      <div class="box">
            <div class="box-header">
                <div class="box-title"> Detalhes da venda - #<?php echo $detalhes['id']; ?></div>
            </div>

    <div class="box-body">
        <div class="container-fluid">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Produtos da venda</h3>
                </div>
            </div>
            <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Produto</th>
                        <th>Preço Unitário</th>
                        <th>Quantidade</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach ($produtoItemVenda as $venda) {
                        ?>
                        <tr>
                            <td><?php echo $venda['produto_id']; ?></td>
                            <td><?php echo $venda['nome']; ?></td>
                            <td><?php echo $venda['venda_preco']; ?></td>
                            <td><?php echo $venda['quant']; ?></td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>

            <form class="form-horizontal" method="post" action="venda-fechar.php">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Resumo da venda</h3>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label for="fid" class="col-sm-2 control-label">#</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['id']; ?></p>
                            </div>

                            <label for="fdata" class="col-sm-2 control-label">Data:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo date('d/m/Y', strtotime($detalhes['data'])); ?></p>
                            </div>

                            <label for="fsituacao" class="col-sm-2 control-label">Tipo:</label>
                                <div class="">
                                <?php if ($detalhes['pagamento'] == 0) { ?>
                                       <?php echo 'Aguardando Pagamento';?>
                                        <?php } elseif($detalhes['pagamento'] == 1) { ?>
                                        <?php echo 'Pago';?>
                                        <?php } elseif($detalhes['pagamento'] == 2) { ?>
                                        <?php echo 'Cancelado';?>
                                    <?php } ?>       
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="fcliente" class="col-sm-2 control-label">Cliente:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['nome']; ?></p>
                            </div>
                            <label for="fcpf" class="col-sm-2 control-label">CPF:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['cpf']; ?></p>
                            </div>
                            <label for="ftelefone" class="col-sm-2 control-label">Telefone:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['telefone']; ?></p>
                            
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fcep" class="col-sm-2 control-label">CEP:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['cep']; ?></p>
                            </div>
                            <label for="flogradouro" class="col-sm-2 control-label">Logradouro:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['logradouro']; ?></p>
                            </div>
                            <label for="fnumero" class="col-sm-2 control-label">Número:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['numero']; ?></p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="fcomplemento" class="col-sm-2 control-label">Complemento:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['complemento']; ?></p>
                            </div>
                             <label for="fbairro" class="col-sm-2 control-label">Bairro:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['bairro']; ?></p>
                            </div>
                             <label for="fcidade" class="col-sm-2 control-label">Cidade:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['cidade']; ?></p>
                            </div>
                             <label for="festado" class="col-sm-2 control-label">Estado:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static"><?php echo $detalhes['estado']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="form-control-static pull-right">Total: R$<span><?php echo number_format($detalhes['total_preco'], 2, ',', '.');?></span></h4>
                </div>

            </div>
        </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/vendas.js"></script>

