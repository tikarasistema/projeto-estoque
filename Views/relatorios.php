<?php

//$mostratelas = $telas[0][0];
$mostratelas = isset($telas[0][0]) ? ($telas[0][0]) : 0;


$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Relatórios <i class="fa fa-paste"></i>
    </h1><br>
</section>
<section class="content container-fluid">
    <!-- /.content -->
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">PESSOAS</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form id="frmUsuario" method="POST" name="registration" action="<?php echo BASE_URL; ?>relatorios/relatorio_usuarios">
                        <div class="info-box bg-default">
                            <span class="info-box-icon"><i class="glyphicon glyphicon-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Relatório de Usuários</span>
                                <span class="info-box-number"></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <p class="progress-description">
                                    <input value="1" type="radio" name="situacao" checked>Ativo
                                    <input value="0" type="radio" name="situacao">Inativo
                                </p>                 
                                <input type="submit" id="btnRelUsuario" target="_blank" name="btnRelUsuario" value="Ir para" class="btn btn-default btn-xs pull-right">
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </form> 
                    <form id="frmRelFornecedores" method="POST" name="registration" action="<?php echo BASE_URL; ?>relatorios/relatorio_fornecedores">
                        <div class="info-box bg-default">
                            <span class="info-box-icon"><i class="fa fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Relatório de Fornecedores</span>
                                <span class="info-box-number"></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <p class="progress-description">
                                    <input value="1" type="radio" name="situacao" checked>Ativo
                                    <input value="0" type="radio" name="situacao">Inativo
                                </p>                 
                                <input type="submit" id="btnRelFornecedor" target="_blank" name="btnRelFornecedor" value="Ir para" class="btn btn-default btn-xs pull-right">
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </form>
                    <form id="frmCliente" method="POST" name="registration" action="<?php echo BASE_URL; ?>relatorios/relatorio_clientes">
                        <div class="info-box bg-default">
                            <span class="info-box-icon"><i class="fa fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Relatório de Clientes</span>
                                <span class="info-box-number"></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <p class="progress-description">
                                    <input value="1" type="radio" name="situacao" checked>Ativo
                                    <input value="0" type="radio" name="situacao">Inativo
                                </p>                 
                                <input type="submit" id="btnRelCliente" target="_blank" name="btnRelCliente" value="Ir para" class="btn btn-default btn-xs pull-right">
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </form>                 
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-barcode"></i>
                    <h3 class="box-title">PRODUTOS</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form id="frmRelCategoria" method="POST" name="registration" action="<?php echo BASE_URL ?>relatorios/relatorio_categorias">
                        <div class="info-box bg-default">
                            <span class="info-box-icon"><i class="fa fa-cube"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Relatório de Categorias</span>
                                <span class="info-box-number"></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <p class="progress-description">
                                    <input value="1" type="radio" name="situacao" checked>Ativo
                                    <input value="0" type="radio" name="situacao">Inativo
                                </p>                 
                                <input type="submit" id="btnRelCategoria" target="_blank" name="btnRelCategoria" value="Ir para" class="btn btn-default btn-xs pull-right">
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </form>
                    <form id="frmProduto" method="POST" name="registration" action="<?php echo BASE_URL; ?>relatorios/relatorio_produtos">
                        <div class="info-box bg-default">
                            <span class="info-box-icon"><i class="fa fa-barcode"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Relatório de Produtos</span>
                                <span class="info-box-number"></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <p class="progress-description">
                                    <input value="1" type="radio" name="situacao" checked>Ativo
                                    <input value="0" type="radio" name="situacao">Inativo
                                </p>                 
                                <input type="submit" id="btnRelProduto" target="_blank" name="btnRelProduto" value="Ir para" class="btn btn-default btn-xs pull-right">
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </form>   
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <!-- SELECT2 EXAMPLE -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-area-chart"></i><h3 class="box-title">RELATÓRIO DE VENDAS</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmVenda" method="POST" name="registration" action="<?php echo BASE_URL; ?>relatorios/relatorio_vendas">
                                <div><label>Filtro:</label></div>
                                <div class="form-group-sm col-md-3">
                                    <label>*Período Inicial </label>
                                    <input type="date" name="dtInicio" id="dtInicio" class="form-control" required="">
                                </div>
                                <div class="form-group-sm col-md-3">
                                    <label>*Período Final </label>
                                    <input type="date" name="dtFinal" id="dtFinal" class="form-control" required="">
                                </div>

                                <div class="form-group-sm col-md-3">
                                    <label>Status da venda</label>
                                    <select name="status_venda" id="status_venda" class="form-control">
                                        <option value="0">Fechada</option>
                                        <option value="1">Aberta</option>
                                        <option value="2">Cancelada</option>
                                    </select>
                                </div>
                                <div class="form-group-sm col-md-3">
                                    <label>Situação do pagamento</label>
                                    <select name="situacao" id="situacao" class="form-control">
                                        <option value="0">Aguardando</option>
                                        <option value="1">Pago</option>
                                        <option value="2">Cancelado</option>
                                    </select>
                                </div>
                                <div class="form-group-sm">
                                    <input type="submit" id="btnRelVenda" target="_blank" name="btnRelVenda" value="Ir para" class="btn btn-default btn-xs pull-right">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>