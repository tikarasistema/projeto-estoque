

<!DOCTYPE html>
<html lang="PT-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Tikara Sistemas | <?php echo $_SESSION['StockPower']['name']; ?></title>
        <!-- Icone -->
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/icon.png">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/css/template.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/skins/skin-blue.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css">

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/alertifyjs/css/alertify.css">
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/alertifyjs/css/themes/default.css">
        <!-- JQuery -->
        <script src="<?php echo BASE_URL ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap-notify.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/alertifyjs/alertify.js"></script>

        <!-- Datatable -->
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>assets/plugins/data.css">
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>assets/plugins/data_bootstrap4.css">
        <script type="text/javascript" src="<?php echo BASE_URL ?>assets/plugins/data.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL ?>assets/plugins/data_bootstrap4.js"></script>
        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">

                <!-- Logo -->
                <a href="<?php echo BASE_URL; ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><i class="fa fa-paw"></i></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-md"><i class="fa fa-paw"></i> Tikara</b>Sistemas</span>
                </a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?php echo BASE_URL; ?>assets/images/avatar.png" class="user-image" alt="Imagem do usuario">
                                    <span class="hidden-xs"><?php echo $_SESSION['StockPower']['name']; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo BASE_URL; ?>assets/images/avatar.png" class="img-circle" alt="Imagem do usuario">
                                        <p>
                                           Usuário:<?php echo $_SESSION['StockPower']['name']; ?>
                                        </p>
                                        <p>
                                           E-mail: <?php echo $_SESSION['StockPower']['email']; ?>
                                        </p>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="<?php echo BASE_URL; ?>login/logout">
                                    <i class="glyphicon glyphicon-off"></i><span class="hidden-xs" style="font-weight: normal;"> Sair</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel">
                        <div class="info">
                            <p> <?php echo $_SESSION['StockPower']['name']; ?></p>
                           
                            <!-- Status -->
                           <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MENU</li>
                        <!-- Optionally, you can add icons to the links -->

                        <li class="<?php echo ($viewData['menuActive'] == 'home') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>home"><i class="fa fa-tachometer-alt"></i> <span>Dashboard</span></a></li>            
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-users"></i>
                                    <span>Pessoa</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php echo ($viewData['menuActive'] == 'usuarios') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>usuarios"><i class="glyphicon glyphicon-user"></i> <span>Usuários</span></a></li>
                                    <li class="<?php echo ($viewData['menuActive'] == 'fornecedores') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>fornecedores"><i class="fa fa-users"></i> <span>Fornecedores</span></a></li>
                                    <li class="<?php echo ($viewData['menuActive'] == 'clientes') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>clientes"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
                                    <li class="<?php echo ($viewData['menuActive'] == 'permissao') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>permissao"><i class="fa fa-lock"></i> <span>Permissão</span></a></li>
                                </ul>
                            </li>
           
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-barcode"></i>
                                    <span>Produto</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php echo ($viewData['menuActive'] == 'categorias') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>categorias"><i class="fa fa-cube"></i> <span>Categorias</span></a></li>
                                    <li class="<?php echo ($viewData['menuActive'] == 'produtos') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>produtos"><i class="fa fa-barcode"></i> <span>Produtos</span></a></li>
                                </ul>
                            </li>
             
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-area-chart"></i>
                                    <span>Movimentação</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php echo ($viewData['menuActive'] == 'vendas') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>vendas"><i class="glyphicon glyphicon-shopping-cart"></i> <span>Vendas</span></a></li>  
                                    <li class="<?php echo ($viewData['menuActive'] == 'compras') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>compras"><i class="fa fa-cart-plus"></i> <span>Compras</span></a></li>
                                </ul>
                            </li>
                     
                       
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-paste"></i>
                                    <span>Relatório</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php echo ($viewData['menuActive'] == 'relatorios') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>relatorios"><i class="fa fa-paste"></i> <span>Relatórios</span></a></li>
                                </ul>
                            </li>
                  
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-pie-chart"></i>
                                    <span>Gráfico</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php echo ($viewData['menuActive'] == 'graficos') ? 'active' : ''; ?>"><a href="<?php echo BASE_URL ?>graficos"><i class="fa fa-pie-chart"></i> <span>Gráficos</span></a></li>        </ul>
                            </li>
                        <li>
                            <a href="<?php echo BASE_URL; ?>login/logout">
                                <i class="glyphicon glyphicon-off"></i><span class="hidden-xs" style="font-weight: normal;"> Sair</span>
                            </a>
                        </li>
                    </ul>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php $this->loadViewInTemplate($viewName, $viewData); ?>
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="pull-right hidden-xs">
                    <strong style="text-transform: uppercase;">Tikara Sistemas</strong>
                </div>
                <!-- Default to the left -->
                <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="" target="_blank">Tikara Sistemas | Marcelo Miyashita</a>.</strong> Todos os Direitos Reservados.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane active" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:;">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:;">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="pull-right-container">
                                            <span class="label label-danger pull-right">70%</span>
                                        </span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 3 -->
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo BASE_URL ?>assets/adminlte/dist/js/adminlte.min.js"></script>
    </body>
</html>