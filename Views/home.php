<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Dashboard <i class="fa fa-tachometer-alt"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid">
    
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa ion-bag"></i></span>

                <div class="info-box-content">
                    <span class="info-box-number">Produtos</span>
                    <span class="info-box-text">Total: <?php echo $qtde_produtos; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-shopping-cart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-number">Vendas</span>
                    <span class="info-box-text">Aberta :  <?php echo $qtVendaAberta; ?></span>
                    <span class="info-box-text">Fechada: <?php echo $qtVendaFechada; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-search-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-number">Despesas com Produtos</span>
                    <span class="info-box-text">
                        R$ <?php echo number_format($total_preco, 2, '.', ','); ?>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-number">Fornecedores</span>
                    <span class="info-box-text"><?php echo $qtde_fornecedores; ?></span>
                </div>
            </div>
        </div>
    </div><br>
    <div class="col-md-8">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Últimas 5 Vendas</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>#</th> 
                                <th>Venda</th>
                                <th>Data</th>
                                <th>Cliente</th>
                                <th>Bruto R$</th>
                                <th>Desc. %</th>
                                <th>Desc. R$</th>
                                <th>Líquido R$</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($lista)) { ?>
                                <?php foreach ($lista as $v) { ?>                  
                                    <tr>
                                        <td><?php echo $v['id']; ?></td>  
                                        <td>
                                            <?php
                                            if ($v['status_venda'] == STATUS_VENDA_FECHADA) {
                                                echo "<div class='situacao_inativo'>Fechada</div>";
                                            } else {
                                                echo "<div class='situacao_ativo'>Aberta</div>";
                                            }
                                            ?> 
                                        </td>
                                        <?php $vendaData = strtotime($v['data']);
                                        ;
                                        ?>
                                        <td><?php echo date('d/m/Y', $vendaData); ?></td>
                                        <td><?php echo $v['nome']; ?></td>
                                        <td>R$ <?php echo str_replace("/", ".", str_replace(",", ".", str_replace(".", "/", number_format($v['total_preco'], 2)))) ?></td>
                                        <td><?php echo $v['desconto']; ?></td>
                                        <td>R$ <?php echo str_replace("/", ".", str_replace(",", ".", str_replace(".", "/", number_format($v['vldesconto'], 2)))) ?></td>
                                        <td>R$ <?php echo str_replace("/", ".", str_replace(",", ".", str_replace(".", "/", number_format($v['totalfinal'], 2)))) ?></td>

                                    </tr>
                                </tbody>
                            <?php } ?>
                        <?php } else { ?>
                            <td  class="text-center" colspan="12">Nenhuma informação a ser exibida</td>
                      <?php } ?>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="<?php echo BASE_URL ?>vendas" class="btn btn-sm btn-info btn-flat pull-right">Ver todos</a>
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>

    <div class="col-md-4">
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Últimos 5 produtos adicionados</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?php foreach ($list as $produto) { ?>

                    <ul class="products-list product-list-in-box">
                        <li class="item">
                            <div class="product-img">
                                <img src="media/produtos/<?php echo $produto['imagem']; ?>"/>
                            </div>
                            <div class="product-info">
                                <a href="javascript:void(0)" class="product-title"><?php echo $produto['categoria']; ?>
                                    <span class="label label-info pull-right">R$ <?php echo number_format($produto['preco'], 2, ',', '.'); ?></span></a>
                                <span class="product-description">
                                <?php echo '#' . $produto['idproduto'] . ' -' . $produto['nome']; ?>
                                </span>
                            </div>
                        </li>
                    </ul>
                    <?php } ?>
            </div>
            <div class="box-footer text-center">
                <a href="<?php echo BASE_URL ?>produtos" class="uppercase">Ver todos os produtos</a>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>