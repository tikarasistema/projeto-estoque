<?php 

//$mostratelas = $telas[0][0];
$mostratelas = isset($telas[0][0]) ? ($telas[0][0]) : 0;

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Vendas <i class="glyphicon glyphicon-shopping-cart"></i>
    </h1>
</section>
<section class="content container-fluid"> 
    <?php
    if (!empty($msg)) {
        echo $msg;
    }
    ?>
    <form method="POST">    

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Cadastrar nova venda</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label><span class="obrigatorio">*</span>Selecione um cliente:</label>

                                <select class="form-control" id=cliente name="cliente" data-alt="Cliente" data-ob="1">
                                    <?php foreach ($clientes as $c): ?>
                                        <option value="<?= $c['idcliente']; ?>"><?= $c['nome']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label><span class="obrigatorio">*</span>Selecione o pagamento:</label>
                            <select class="form-control" id="situacao" name="situacao" data-alt="situacao" data-ob="1">
                                <option value="0">Aguardando Pgto.</option>
                                <option value="1">Pago</option>
                                <option value="2">Cancelado</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
<!--                    <div class="col-md-6">

                            <div class="form-group">
                                <label>*Desconto </label>
                                <input type="number" name="desconto" id="desconto" min="1" class="form-control" required="">
                            </div>            
                       
                    </div>-->
                     
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                * Campos obrigatórios
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <h4 class="box-title">Adicionar produtos na venda</h4>
                        <div></div>
                        <div>
                            <input autocomplete="off" name="table_search" data-type="search_produtos" id="add_prod" class="form-control pull-right" placeholder="Busque pelo produto...">
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="box2">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="produtos_table">
                            <tr>
                                <th>ID</th>
                                <th>Nome do Produto</th>
                                <th>Quantidade</th>
                                <th>Preço Unit.</th>
                                <th>Sub-Total</th>
                                <th>Ação</th>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="box-footer">
                        <div class="col-xs-6 pull-right">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:70%">Sub Total R$:</th>
                                        <td><input type="text" class="form-control" name="total_price" id="total_price" disabled="disabled"></td>
                                    </tr>
           
                                    <tr>
                                        <th>Desconto %</th>
                                        <td><input type="number" class="form-control" name="cpdesconto" min="0" id="cpdesconto"></td>
                                    </tr>
                                    <tr>
                                        <th>Desconto R$:</th>
                                        <td><input name="vldesconto" class="form-control" id="vldesconto" disabled="disabled"></td>
                                    </tr>
                                    <tr>
                                        <th>TOTAL R$:</th>
                                        <td><input type="text" name="totalfinal" class="form-control" id="totalfinal" disabled="disabled"></td>
                                    </tr>
                                </table>
                            </div>                
                <div class="row no-print">
                    <div class="col-xs-12">
                        <button type="reset" class="btn-sm btn-danger pull-right"><i class="fa fa-refresh"></i> Limpar
                        </button>
                        <button type="submit" id="btnAdicionar" class="btn-sm btn-info pull-right"><i class="fa fa-credit-card"></i> Adicionar Venda
                        </button>
                    </div>
                </div>
            </div>
                </div>
            </div>
        </div>
    </form>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/vendas_add.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

