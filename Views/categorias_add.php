<?php 

//$mostratelas = $telas[0][0];
$mostratelas = isset($telas[0][0]) ? ($telas[0][0]) : 0;
;
$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Categorias <i class="fa fa-cube"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
<?php if(!empty($msg)) {echo $msg;} ?>
    <form id="frmCategoria" method="POST" name="registration" action="<?php echo BASE_URL ?>categorias/add">

        <div class="box">
            <div class="box-header">
                <div class="box-title"> Adicionar nova Categoria</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Categoria</label>
                            <input type="text" name="categoria" id="categoria" class="form-control" >
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <select name="situacao" id="situacao" class="form-control">
                                <option value="">Selecione...</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>                 
                </div>
            </div>

            <div class="box-tools">
                <input type="submit" id="btnAdicionar" name="adicionarCategoria" value="Salvar" class="btn btn-primary pull-right">
            </div>
        </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/categorias.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>
<script>
    function validarFormVazio(formulario) {
        dados = $('#' + formulario).serialize();
        d = dados.split('&');
        vazios = 0;
        for (i = 0; i < d.length; i++) {
            controles = d[i].split("=");
            if (controles[1] == "A" || controles[1] == "") {
                vazios++;
            }
        }
        return vazios;
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdicionar').click(function () {

            vazios = validarFormVazio('frmCategoria');

            if (vazios > 0) {
                alertify.alert("Preencha os campos.");
                return false;
            }
        });
    });
</script>