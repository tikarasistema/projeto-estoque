<?php

$mostratelas = implode(",", $telas[0]);

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$resultado = substr($url_atual,0,-2);
$pattern = ',' . $resultado . ',';
;?>

<?php if (preg_match($pattern, $mostratelas)) { ?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/produto-edit.css">
<section class="content-header">
    <h1>
        Produtos <i class="fa fa-barcode"></i>
    </h1>
</section>

<!-- Main content -->
<a href="produtos_edit.php"></a>
<section class="content container-fluid"> 
<?php if(!empty($msg)) {echo $msg;} ?>
    <form method="POST" name="registration" action="<?php echo BASE_URL ?>produtos/edit/<?php echo $produto_info['idproduto'] ?>" enctype="multipart/form-data">

        <div class="box">
            <div class="box-header">
                <div class="box-title"> Editar dados do produto - #<?php echo $produto_info['idproduto']; ?></div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $produto_info['nome'] ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Preço</label>
                            <input type="text" name="preco" id="preco" class="form-control" value="<?php echo $produto_info['preco'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Quantidade</label>
                            <input type="qtde" name="qtde" id="qtde" class="form-control"value="<?php echo $produto_info['qtde'] ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Quantidade mínima</label>
                            <input type="text" name="qtde_min" id="qtde_min" class="form-control" value="<?php echo $produto_info['qtde_min'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>*Categorias</label>
                            <select name="categoria" id="categoria" class="form-control">
                                <?php foreach ($categoria_list as $categoria) : ?>
                                    <option value="<?php echo $categoria['categoria']; ?>" <?php echo ($categoria['categoria'] == $produto_info['categoria']) ? 'selected="selected"' : ''; ?>> <?php echo $categoria['categoria']; ?></option>
                                <?php endforeach; ?>      
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>*Fornecedores</label>
                            <select name="fornecedor" id="fornecedor" class="form-control">
                                <?php foreach ($fornecedor_list as $fornecedor) : ?>
                                    <option value="<?php echo $fornecedor['nome']; ?>" <?php echo ($fornecedor['nome'] == $produto_info['fornecedor']) ? 'selected="selected"' : ''; ?>> <?php echo $fornecedor['nome']; ?></option>
                                <?php endforeach; ?>      
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6">            
                        <div class="form-group">
                            <label for="imagem">Imagem opcional <i class="fa fa-images"></i></label><br>
                            <?php
                            $img = "media/produtos/" . $produto_info['imagem'];

                            if (file_exists($img) && $produto_info['imagem'] !== ''):
                                ?>
                                <a onclick="" class="btn btn-primary btn-md buttonImg">Mostrar/ Ocultar <i class="fa fa-eye"></i></a>
                                <br>
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <img class="img-fluid imagem-foto" src="<?php echo BASE_URL; ?>media/produtos/<?php echo $produto_info['imagem'] ?>"><br><br>
                                </div>
                            <?php else: ?>
                                <span class="btn btn-warning">Nenhuma Imagem <i class="fa fa-images"></i></span><br><br>
<?php endif; ?>

                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <div class="checkbox">
                                <label for="situacao">
                                    <input type="checkbox" name="situacao" id="situacao"
                                <?php if ($produto_info['situacao'] == 1) { ?> checked<?php } ?>
                                           > Produto ativo
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-tools">
                    <input type="submit" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/produtos.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>


