<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tikara Sistemas | <?php echo $_SESSION['StockPower']['name']; ?></title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/AdminLTE.min.css">
    
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

</head>
<section class="content container-fluid">
    <div class="panel panel-info">
        <div class="panel-heading text-center"><h4><strong>RELATÓRIO</strong> DE FORNECEDORES</h4></div>
        <div class="panel-body">
            <div class="table-responsive">
                <diV>
                    <input type="button" class="btn btn-success pull-right" value="Imprimir" onclick="printpage()"/>
                </diV>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Situação</th>
                        <th>Nome</th>
                        <th>Sobrenome</th>
                        <th>CPF</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>CEP</th>
                        <th>UF</th>
                        <th>Cidade</th>
                        <th>Logradouro</th>
                        <th>No.</th>
                        <th>Complemento</th>
                        <th>Bairro</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($list)) { ?>
                    <?php foreach ($list as $c) { ?>
                        <tr>
                            <td><?php echo $c['idcliente']; ?></td>
                            <td>
                                <?php
                                if ($c['situacao'] == 0) {
                                    echo "<div class='situacao_inativo'>Inativo</div>";
                                } else {
                                    echo "<div class='situacao_ativo'>Ativo</div>";
                                }
                                ?> 
                            </td>
                            <td><?php echo $c['nome']; ?></td>
                            <td><?php echo $c['sobrenome']; ?></td>
                            <td><?php echo $c['cpf']; ?></td>
                            <td><?php echo $c['email']; ?></td>
                            <td><?php echo $c['telefone']; ?></td>
                            <td><?php echo $c['cep']; ?></td>
                            <td><?php echo $c['estado']; ?></td>
                            <td><?php echo $c['cidade']; ?></td>
                            <td><?php echo $c['logradouro']; ?></td>
                            <td><?php echo $c['numero']; ?></td>
                            <td><?php echo $c['complemento']; ?></td>
                            <td><?php echo $c['bairro']; ?></td>
                            <?php } ?>
                        
                           <?php } else { ?><tr>
                                <td  class="text-center" colspan="15">Nenhuma informação a ser exibido</td>
                           </tr> <?php } ?>
                    </tbody>
            </table>
        </div>
        </div>
        <div class="panel-footer"> <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="" target="_blank">Tikara Sistemas | Marcelo Miyashita</a>.</strong> Todos os Direitos Reservados.</div>
    </div>
</section>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/relatorios.js"></script>
