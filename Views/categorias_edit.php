<?php

$mostratelas = implode(",", $telas[0]);

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$resultado = substr($url_atual,0,-2);
$pattern = ',' . $resultado . ',';
;?>

<?php if (preg_match($pattern, $mostratelas)) { ?>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Categorias <i class="fa fa-cube"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
    <?php if(!empty($msg)) {echo $msg;} ?>
    <form method="POST" name="registration" action="<?php echo BASE_URL ?>categorias/edit/<?php echo $categoria_info['idcategoria'] ?>">
        <div class="box">
            <div class="box-header">
                <div class="box-title"> Editar dados da Categoria - #<?php echo $categoria_info['idcategoria']; ?></div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Categoria</label>
                            <input type="text" name="categoria" id="categoria" class="form-control" value="<?php echo $categoria_info['categoria'] ?>">
<!--                            <span class='msg-erro msg-nome'></span>-->
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <div class="checkbox">
                                <label for="situacao">
                                    <input type="checkbox" name="situacao" id="situacao"
                                           <?php if ($categoria_info['situacao'] == 1) { ?> checked<?php } ?>
                                           > Categoria ativa
                                </label>
                            </div>
                        </div>
                    </div>
                </div>              
                </div>
                <div class="box-tools">
                    <input type="submit" id="success-btn" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>
