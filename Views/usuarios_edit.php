<?php

$mostratelas = implode(",", $telas[0]);

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$resultado = substr($url_atual,0,-2);
$pattern = ',' . $resultado . ',';
;?>

<?php if (preg_match($pattern, $mostratelas)) { ?>

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
    <!-- Conteudo da Pagina -->
    <section class="content-header">
        <h1>
            Usuários <i class="glyphicon glyphicon-user"></i>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid"> 
        <?php
        if (!empty($msg)) {
            echo $msg;
        }
        ?>
        <form method="POST" name="registration" action="<?php echo BASE_URL ?>usuarios/edit/<?php echo $info['id'] ?>">
            <div class="box">
                <div class="box-header">
                    <div class="box-title"> Editar dados do Usuário - #<?php echo $info['id']; ?></div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>*Nome </label>
                                <input type="text" name="name" id="name" class="form-control" value="<?php echo $info['name'] ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>*E-mail</label>
                                <input type="text" name="email" id="email" class="form-control" value="<?php echo $info['email'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>*Senha</label>
                                <input type="password" name="password" id="password" class="form-control" value="<?php echo $info['password'] ?>">
                            </div>
                        </div>

                        <div class="col-md-6"> 
                            <div class="form-group">
                                <label>Nível de Acesso</label>
                                <select class="form-control" name="nivel" id="nivel">
                                    <?php foreach ($permissao as $p) { ?>
                                        <option value="<?php echo $p['id_permissao']; ?>" <?php echo $info['nivel'] == $p['id_permissao'] ? 'selected' : ''; ?>><?php echo $p['descricao']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ROW">
                        <div class="col-md-6"> 
                            <div class="form-group">
                                <label>Situação</label>
                                <div class="checkbox">
                                    <label for="situacao">
                                        <input type="checkbox" name="situacao" id="situacao"
                                    <?php if ($info['situacao'] == SITUACAO_ATIVO) { ?> checked<?php } ?>
                                               > Usuário ativo
                                    </label>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="box-tools">
                    <input type="submit" id="success-btn" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>
            </div>
        </form>
        <p>Campos Obrigatórios (*)</p>
    </section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/usuarios.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

