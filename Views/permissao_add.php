<?php require './assets/helpers/telas.php'; ?>


<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Permissão <i class="fa fa-lock"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
    <?php
    if (!empty($msg)) {
        echo $msg;
    }
    ?>
    <form id="frmPermissao" method="POST" name="registration" action="<?php echo BASE_URL ?>permissao/add_action">

        <div class="box">
            <div class="box-header">
                <div class="box-title"> Adicionar nova Permissão</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Descrição</label>
                            <input type="text" name="descricao" id="descricao" placeholder="Informe um nome para permissão" class="form-control" required="true" >
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <select name="situacao" id="situacao" class="form-control">
                                <option value="">Selecione...</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <div class="panel-body">
                                <div class="row">
<?php foreach ($telas as $tela) { ?>
                                        <div class="col-xs-12 col-sm-6 col-md-3">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h1 class="panel-title"><?php echo $tela['titulo']; ?></h1>
                                                </div>
                                                <div class="panel-body">
    <?php foreach ($tela['telas'] as $link => $titulo) { ?>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="<?php echo $link; ?>" <?php echo (in_array($link, explode(',', $telasPermissao))) ? 'checked' : ''; ?> name="urls[]">
        <?php echo " $titulo"; ?>
                                                            </label>
                                                        </div><?php } ?>
                                                </div>
                                            </div>
                                        </div>
<?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="box-tools">
                    <input type="submit" id="btnAdicionar" name="adicionarPermissao" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>

        </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>

<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/permissao.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>
<script>
    function validarFormVazio(formulario) {
        dados = $('#' + formulario).serialize();
        d = dados.split('&');
        vazios = 0;
        for (i = 0; i < d.length; i++) {
            controles = d[i].split("=");
            if (controles[1] == "A" || controles[1] == "") {
                vazios++;
            }
        }
        return vazios;
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdicionar').click(function () {

            vazios = validarFormVazio('frmPermissao');

            if (vazios > 0) {
                alertify.alert("Preencha os campos.");
                return false;
            }
        });
    });
</script>