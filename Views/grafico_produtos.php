<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Gráficos</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body>

        <!-- Main content -->
        <section class="content">
            <div class="section">
                <div class="col-md-6">            
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Produtos adicionados mês/ano</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="barChart" style="height:500px"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            foreach ($total_mes as $produto) {
                $label[] = 'Mês ' . $produto['mes'] . ' /' . $produto['ano'] . ' - ' . $produto['nome'];
                $data[] = $produto['total'];
            }
            ?>
        </section>
        
        <script src="<?php echo BASE_URL ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo BASE_URL ?>assets/adminlte/bower_components/chart.js/Chart.js"></script>
        <script src="<?php echo BASE_URL ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
        <script src="<?php echo BASE_URL ?>assets/adminlte/dist/js/adminlte.min.js"></script>
        <script src="<?php echo BASE_URL ?>assets/adminlte/dist/js/demo.js"></script>

        <!-- page script -->
        <script>
            var dados = <?php echo json_encode($data); ?>;
            var label = <?php echo json_encode($label); ?>;
        </script>
        <script>

            $(function () {
                // Get context with jQuery - using jQuery's .get() method.
                var areaChartCanvas = $('#barChart').get(0).getContext('2d')
                // This will get the first returned node in the jQuery collection.
                var areaChart = new Chart(areaChartCanvas)

                var areaChartData = {
                    labels: label,
                    datasets: [
                        {
                            label: 'Qtde',
                            fillColor: '#00c0ef ',
                            strokeColor: '#fff',
                            pointColor: '#c1c7d1',
                            pointStrokeColor: '#c1c7d1',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(220,220,220,1)',
                            data: dados
                        }

                    ]
                }

                var barChartCanvas = $('#barChart').get(0).getContext('2d')
                var barChart = new Chart(barChartCanvas)
                var barChartData = areaChartData
                var areaChartData = {
                }

                var barChartOptions = {
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: true,
                    //String - Colour of the grid lines
                    scaleGridLineColor: 'rgba(0,0,0,.05)',
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke: true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth: 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing: 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing: 1,
                    //String - A legend template
                    legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                    //Boolean - whether to make the chart responsive
                    responsive: true,
                    maintainAspectRatio: true
                }
                barChart.Bar(barChartData, barChartOptions)
            })
        </script>
    </body>
</html>
