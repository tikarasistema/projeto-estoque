<?php

$mostratelas = implode(",", $telas[0]);

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$resultado = substr($url_atual,0,-2);
$pattern = ',' . $resultado . ',';
;?>

<?php if (preg_match($pattern, $mostratelas)) { ?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Vendas <i class="glyphicon glyphicon-shopping-cart"></i>
    </h1>
</section>
<section class="content container-fluid"> 
    <?php
    if (!empty($msg)) {
        echo $msg;
    }
    ?>
    <form method="POST" name="registration" action="<?php echo BASE_URL ?>vendas/edit/<?php echo $venda['info']['id'] ?>"> 

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Editar nova venda # <?php echo $venda['info']['id']; ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span class="obrigatorio"></span>Data:</label>
                            <input class="form-control" readonly type="text" name="data" id="data" value="<?php echo date('d/m/Y', strtotime($venda['info']['data'])); ?>">
                        </div>
                        <div class="form-group">
                            <label><span class="obrigatorio"></span>Cliente:</label>
                            <select class="form-control" id=cliente name="cliente" data-alt="Cliente" data-ob="1">
                                <?php foreach ($clientes as $c): ?>
                                    <option value="<?= $c['idcliente']; ?>" <?= $venda['info']['cliente_id'] == $c['idcliente'] ? "selected" : ""; ?>><?= $c['nome']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Desconto </label>
                            <?php if (!empty($venda['item_produtos'])) { ?>
                                <input type="number" readonly name="desconto" id="desconto" class="form-control" value="<?php echo $venda['info']['desconto'] ?>">
                            <?php } else { ?>
                                <input type="number" name="desconto" id="desconto" class="form-control" value="<?php echo $venda['info']['desconto'] ?>">
                            <?php } ?>
                        </div>            
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label><span class="obrigatorio">*</span>Selecione o pagamento:</label>
                            <select class="form-control" id="esconderBotao" name="situacao" onchange="esconderBtn()">
                                <?php foreach ($statuses as $statusKey => $statusValue): ?>
                                    <option value="<?php echo $statusKey; ?>" <?php echo ($statusKey == $venda['info']['situacao']) ? 'selected="selected"' : ''; ?>><?php echo $statusValue; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                * Campos obrigatórios
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover table-sm" id="produtos_table">
                            <tr>
                                <th>ID</th>
                                <th>Nome do Produto</th>
                                <th>Quantidade</th>
                                <th>Preço Unitário</th>
                                <th>Preço Total</th>
                            </tr>

                            <?php if (!empty($venda['item_produtos'])) { ?>
                                <?php foreach ($venda['item_produtos'] as $productitem) { ?>
                                    <?php $total = $productitem['total_preco']; ?>
                                    <tr>
                                        <td><?php echo $productitem['venda_id']; ?></td>
                                        <td><?php echo $productitem['nome']; ?></td>
                                        <td><?php echo $productitem['quant']; ?></td>
                                        <td>R$ <?php echo number_format($productitem['venda_preco'], 2, ',', '.'); ?></td>
                                        <td>R$ <?php echo number_format($productitem['venda_preco'] * $productitem['quant'], 2, ',', '.'); ?></td> 
                                    </tr>
                                <?php }; ?>
                            <?php } else { ?><tr>
                                    <td  class="text-center" colspan="6">Nenhum produto a ser informado</td>
                                </tr> <?php } ?>
                        </table>
                        <?php if (empty($venda['item_produtos'])) { ?>
                            <div>
                            <?php } else { ?>
                                <div class="col-xs-12">
                                    <p id="btndelete"><a class="btn btn-danger pull-right btn-sm" href="<?php echo BASE_URL . 'vendas/delete/' . $productitem['venda_id'] ?>"><i class="fa fa-trash"></i> Excluir</a></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if (empty($venda['item_produtos'])) { ?>
                        <div class="box">

                            <div class="box-body">
                                <h4 class="box-title">Adicionar produtos na venda</h4>
                                <div></div>
                                <div>
                                    <input autocomplete="off" name="table_search" data-type="search_produtos" id="add_prod" class="form-control pull-right" placeholder="Busque pelo produto...">
                                </div>
                            </div>

                            <div class="box">
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" id="produtos_table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Nome do Produto</th>
                                            <th>Quantidade</th>
                                            <th>Preço Unit.</th>
                                            <th>Sub-Total</th>
                                            <th>Ação</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        <?php } else { ?><p>
                            <p  class="text-center" colspan="6">Nenhum produto a ser adicionado</p>
                            </p> <?php } ?>
                        <!-- /.box -->
                    </div>
                    <div class="box-footer">
                        <div class="col-xs-6 pull-right">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:70%">Total Bruto:</th>
                                    <input type="hidden" value="<?php echo $total_preco = $productitem['total_preco']; ?>">
                                    <td><input type="text" name="total_price" id="total_price" disabled="disabled" value="<?php echo number_format($total_preco, 2, ',', '.'); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Desconto %</th>
                                    <input type="hidden" value="<?php echo $desconto = $productitem['desconto']; ?>">
                                    <td><input type="text" name="desconto" id="desconto" disabled="disabled" value="<?php echo number_format($desconto); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Desconto R$:</th>
                                    <input type="hidden" value="<?php echo $vldesconto = $productitem['total_preco'] - ($productitem['totalfinal']); ?>">
                                    <td><input name="vldesconto" id="vldesconto" disabled="disabled" value="<?php echo number_format($vldesconto, 2, ',', '.'); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                    <input type="hidden" value="<?php echo $totalfinal = $productitem['totalfinal']; ?>">
                                    <td><input type="text" name="totalfinal" id="totalfinal" disabled="disabled" value="<?php echo number_format($totalfinal, 2, ',', '.'); ?>"></td>
                                    </tr>
                                </table>
                            </div>                
                            <div class="row no-print">
                                <div class="col-xs-12">
                                    <button type="reset" class="btn btn-danger pull-right"><i class="fa fa-refresh"></i> Limpar
                                    </button>
                                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Editar Venda
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/vendas_editar.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

<script type="text/javascript">
function esconderBtn() {
    var status = document.getElementById("esconderBotao").value;

        if (status == 0) {
            document.getElementById("btndelete").style.display = 'block';
        } else if (status == 2) {
            document.getElementById("btndelete").style.display = 'none';
        } else {
            document.getElementById("btndelete").style.display = 'none';
        }
    }
</script>