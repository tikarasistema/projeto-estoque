<?php 

$mostratelas = $telas[0][0];

$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>
	
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Clientes <i class="fa fa-users"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
<?php if(!empty($msg)) {echo $msg;} ?>
    <form id="frmCliente" method="POST" name="registration" action="<?php echo BASE_URL ?>clientes/add">

        <div class="box">
            <div class="box-header">
                <div class="box-title"> Adicionar novo Cliente</div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Sobrenome</label>
                            <input type="text" name="sobrenome" id="sobrenome" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*E-mail</label>
                            <input type="email" name="email" id="email" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Telefone</label>
                            <input type="text" name="telefone" id="telefone" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*CPF</label>
                            <input type="text" name="cpf" id="cpf" maxlength="14" class="form-control" onBlur="validaFormato(this);" onkeypress="return (apenasNumeros(event))" required="">
                            <div id="divResultado"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*CEP</label>
                            <input type="number" name="cep" id="cep" maxlength="18" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-6">            
                        <div class="form-group">
                            <label>*Logradouro</label>
                            <input type="text" name="logradouro" id="logradouro" class="form-control"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Número</label>
                            <input type="text" name="numero" id="numero" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Complemento</label>
                            <input type="text" name="complemento" id="complemento" class="form-control">    
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>*Bairro</label>
                            <input type="text" name="bairro" id="bairro" class="form-control">   
                        </div>
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-6">            
                        <div class="form-group">
                            <label>*Estado</label>
                            <input type="text" name="estado" id="estado" class="form-control"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>*Cidade</label>
                            <input type="text" name="cidade" id="cidade" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <select name="situacao" id="situacao" class="form-control">
                                <option value="">Selecione...</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="box-tools">
                    <input id="btnAdicionar" name="adicionarCliente" type="submit" value="Salvar" class="btn btn-primary pull-right">
                </div>
            </div>
        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/clientes.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#cep").on("change", function () {
            if (this.value) {
                $.ajax({
                    url: 'http://api.postmon.com.br/v1/cep/' + this.value,
                    dateType: "json",
                    crossDomain: true,
                    statusCode: {
                        200: function (data) {
                            //console.log(data);

                            $("#cep").addClass("is-valid");
                            $("#endereco").val(data.endereco);
                            //$("#bairro").val(data.bairro);
                            $("#cidade").val(data.cidade);
                            $("#estado").val(data.estado);
                        },
                        400: function (msg) {
                            console.log(msg); //Request error
                        },
                        404: function (msg) {
                            console.log(msg); //Cep inválido
                        }
                    }
                })
            }
        });
    });
</script>
<script>
    function validarFormVazio(formulario) {
        dados = $('#' + formulario).serialize();
        d = dados.split('&');
        vazios = 0;
        for (i = 0; i < d.length; i++) {
            controles = d[i].split("=");
            if (controles[1] == "A" || controles[1] == "") {
                vazios++;
            }
        }
        return vazios;
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdicionar').click(function () {

            vazios = validarFormVazio('frmCliente');

            if (vazios > 0) {
                alertify.alert("Preencha os campos.");
                return false;
            }
        });
    });
</script>