<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tikara Sistemas | <?php echo $_SESSION['StockPower']['name']; ?></title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo BASE_URL ?>assets/adminlte/dist/css/AdminLTE.min.css">
    
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

</head>
<section class="content container-fluid">
    <div class="panel panel-info">
        <div class="panel-heading text-center"><h4><strong>RELATÓRIO</strong> DE PRODUTOS</h4></div>
        <div class="panel-body">
            <div class="table-responsive">
                <diV>
                    <input type="button" class="btn btn-success pull-right" value="Imprimir" onclick="printpage()"/>
                </diV>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Situação</th>
                        <th>Imagem</th>
                        <th>Preço</th>
                        <th>Qtde. atual</th>
                        <th>Qtde. mínima</th>
                        <th>Categoria</th>
                        <th>Fornecedor</th>

                    </tr>
                </thead>
                <tbody>
                     <?php if (!empty($list)) { ?>
                    <?php foreach ($list as $produto) { ?>
                        <tr>
                            <td><?php echo $produto['idproduto']; ?></td>

                            <td><?php echo $produto['nome']; ?></td>
                            <td>
                                <?php
                                if ($produto['situacao'] == SITUACAO_INATIVO) {
                                    echo "<div class='situacao_inativo'>Inativo</div>";
                                } else {
                                    echo "<div class='situacao_ativo'>Ativo</div>";
                                }
                                ?> 
                            </td>
                            <td style="width:5%"><img src="../media/produtos/<?php echo $produto['imagem']; ?>"style="width:100%" /></td>
                            <td><?php echo number_format($produto['preco'], 2, ',', '.'); ?></td>
                            <td><?php echo $produto['qtde']; ?></td>
                            <td><?php echo $produto['qtde_min']; ?></td>
                            <td><?php echo $produto['categoria']; ?></td>
                            <td><?php echo $produto['fornecedor']; ?></td>
                        <?php } ?>
                        <?php } else { ?><tr>
                                <td  class="text-center" colspan="15">Nenhuma informação a ser exibido</td>
                           </tr> <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
        <div class="panel-footer"> <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="" target="_blank">Tikara Sistemas | Marcelo Miyashita</a>.</strong> Todos os Direitos Reservados.</div>
    </div>
</section>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/relatorios.js"></script>
