
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Permissão <i class="fa fa-lock"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <div class="box-title"> Listagem de Permissão</div>
            <div class="box-tools">
                <a href="<?php echo BASE_URL ?>permissao/add" class="btn btn-success"> + Adicionar</a>
            </div> 
        </div>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Situação</th>
                        <th>Descrição</th>
                        <th>Página de acesso</th>
                        <th class="actions">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list as $p) { ?>
                        <tr>
                            <td><?php echo $p['id_permissao']; ?></td>
                            <td>
                                <?php
                                if ($p['situacao'] == SITUACAO_INATIVO) {
                                    echo "<div class='situacao_inativo'>Inativo</div>";
                                } else {
                                    echo "<div class='situacao_ativo'>Ativo</div>";
                                } ;?> 
                            </td>
                            <td><?php echo $p['descricao']; ?></td>
                            <td><?php echo $p['telas']; ?></td>
                            <td>
                                <a class="btn btn-success btn-xs" href="<?php echo BASE_URL . 'permissao/edit/' . $p['id_permissao'] ?>">Editar</a>
<!--                                <a class="open-excluirRegistro btn btn-danger btn-xs" data-delete-registro="<?php echo $p['id_permissao']; ?>"  href="" onClick="ShowModal(this)" data-toggle="modal" data-target="#delete-modal">Excluir</a>-->
                            </td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalLabel">Excluir Registro</h4>
            </div>
            <div class="modal-body">
                Deseja realmente excluir este registro?
            </div>
            <div class="modal-footer">
                <a id="acaoRemoverRegistro" class="btn btn-danger" href="#">Sim</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/permissao.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

