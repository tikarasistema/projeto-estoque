<?php require './assets/helpers/telas.php'; ?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<!-- Conteudo da Pagina -->
<section class="content-header">
    <h1>
        Permissão <i class="fa fa-lock"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid"> 
    <?php
    if (!empty($msg)) {
        echo $msg;
    }
    ?>
    <form method="POST" name="registration" action="<?php echo BASE_URL ?>permissao/edit/<?php echo $permissao_info['id_permissao'] ?>">

        <div class="box">
            <div class="box-header">
                <div class="box-title"> Editar dados da Permissão - #<?php echo $permissao_info['id_permissao']; ?></div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>*Descrição</label>
                        <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $permissao_info['descricao'] ?>">
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Situação</label>
                            <div class="checkbox">
                                <label for="situacao">
                                    <input type="checkbox" name="situacao" id="situacao"
                                           <?php if ($permissao_info['situacao'] == SITUACAO_ATIVO) { ?> checked<?php } ?>
                                           > Permissão ativa
                                </label>
                            </div>
                        </div>
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <div class="panel panel-default">
                               
                                <div class="panel-body">
                                    <div class="row">
                                        <?php foreach ($telas as $tela) { ?>
                                            <div class="col-xs-12 col-sm-6  col-md-3">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h1 class="panel-title"><?php echo $tela['titulo']; ?></h1>
                                                    </div>
                                                    <div class="panel-body">
                                                        <?php
                                                        foreach ($tela['telas'] as $link => $titulo) {
                                                            ?>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="<?php echo $link; ?>" <?php
                                                                    if ($telasPermissao) {
                                                                        echo (in_array($link, explode(',', $telasPermissao))) ? 'checked' : '';
                                                                    } else {
                                                                        echo(in_array($link, explode(',', $permissao_info['telas']))) ? 'checked' : '';
                                                                    }
                                                                    ?> name="urls[]">
                                                            <?php echo " $titulo"; ?>
                                                                </label>
                                                            </div>
                                                            <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="box-tools">
            <input type="submit" id="btnAdicionar" name="editarPermissao" value="Salvar" class="btn btn-primary pull-right">
        </div>
            </div>

        </div>
    </form>
    <p>Campos Obrigatórios (*)</p>
</section>

<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/permissao.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>
<script>
    function validarFormVazio(formulario) {
        dados = $('#' + formulario).serialize();
        d = dados.split('&');
        vazios = 0;
        for (i = 0; i < d.length; i++) {
            controles = d[i].split("=");
            if (controles[1] == "A" || controles[1] == "") {
                vazios++;
            }
        }
        return vazios;
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdicionar').click(function () {

            vazios = validarFormVazio('frmCategoria');

            if (vazios > 0) {
                alertify.alert("Preencha os campos.");
                return false;
            }
        });
    });
</script>