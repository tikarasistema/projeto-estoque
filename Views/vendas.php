<?php

//$mostratelas = $telas[0][0];
$mostratelas = isset($telas[0][0]) ? ($telas[0][0]) : 0;


$url = $_SERVER['REQUEST_URI'];
$explode = explode(",", $url);
$tamanho = count($explode) - 1;
$url_atual = $explode[$tamanho];

$verificatela = explode(',', $mostratelas);
;?>

<?php if (in_array($url_atual, $verificatela)) { ?>
<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/pace.css">
<section class="content-header">
    <h1>
        Vendas <i class="glyphicon glyphicon-shopping-cart"></i>
    </h1>
</section>

<!-- Main content -->
<section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <div class="box-title"> Listagem de Vendas</div>
            <div class="box-tools">
                <a href="<?php echo BASE_URL ?>vendas/add" class="btn btn-success"> + Adicionar</a>
            </div> 
        </div>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Venda</th>
                        <th>Clientes</th>
                        <th>Data</th>
                        <th>Pagamento</th>
                        <th>Bruto R$</th>
                        <th>Desc. %</th>
                        <th>Desc. R$</th>
                        <th>Líquido R$</th>
                        <th class="actions">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($venda_lista as $v) { ?>
                        <tr>
                            <td><?php echo $v['id']; ?></td>
                            <td>
                                <?php
                                if ($v['status'] == STATUS_VENDA_FECHADA) {
                                    echo "<div class='situacao_inativo'>Fechada</div>";
                                } else {
                                    echo "<div class='situacao_ativo'>Aberta</div>";
                                } ;?> 
                            </td>
                            <td><?php echo $v['nome']; ?></td>
                            <td><?php echo date('d/m/Y', strtotime($v['data'])); ?></td>
                            <td>
                                <?php if ($v['pagamento'] == AGUARDANDO_PAGAMENTO) { ?>
                                    <?php echo 'Aguardando Pgto'; ?>
                                <?php } elseif ($v['pagamento'] == PAGO) { ?>
                                    <?php echo 'Pago'; ?>
                                <?php } elseif ($v['pagamento'] == CANCELADO) { ?>
                                    <?php echo 'Cancelado'; ?>
                                <?php } ?>   
                            </td>
                            <td><?php echo $v['total_preco']; ?></td>
                            <td><?php echo $v['desconto']; ?></td>
                            <td><?php echo $v['vldesconto']; ?></td>
                            <td><?php echo $v['totalfinal']; ?></td>
                            <td>
                                <!-- status da venda - 0->fechada/1->aberta -->
                                <?php if ($v['status'] == STATUS_VENDA_FECHADA) { ?>
                                    <a class="btn btn-success btn-xs" title="Detalhes venda" href="<?php echo BASE_URL . 'vendas/venda_detalhes/' . $v['id'] ?>"><i class="fa fa-list"></i> Detalhes</a>
                                    <a class="btn btn-warning btn-xs" title="Estornar venda" href="<?php echo BASE_URL . 'vendas/venda_estornar/' . $v['id']  ?>"><i class="fa fa-undo"></i> Estornar</a>

                                <?php } elseif ($v['status'] == STATUS_VENDA_ABERTA) { ?>
                                    <a class="btn btn-info btn-xs" title="Editar venda" href="<?php echo BASE_URL . 'vendas/edit/' . $v['id'] ?>"><i class="fa fa-edit"></i> Editar</a>
                                <?php } elseif ($v['status'] == 2) { ?>
                                    <a class="btn btn-success btn-xs" title="Venda fechada" href="<?php echo BASE_URL . 'vendas/venda_fechada/' . $v['id'] ?>"><i class="fa fa-list"></i> Detalhes</a>
                            <?php } ?>   
                         <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php } else { ?>
    <div class="callout callout-danger">
        <h4>Acesso negado!</h4>
        <p>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique para voltar</a>';
            }
            ?>
        </p>
        <p><?php echo "Você não tem permissão para acessar esta tela.  $voltar"; ?></p>
    </div>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/vendas.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL ?>assets/lib/pace.js"></script>

