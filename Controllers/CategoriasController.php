<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Categorias;
use \Models\Permissao;

class CategoriasController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'categorias'
        );
    }

    public function index() {

        $categorias = new Categorias();
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao(); 
        $this->arrayInfo['list'] = $categorias->getList();
        $this->loadTemplate('categorias', $this->arrayInfo);
    }

//    public function add() {
//        
//        $p = new Permissao();
//         
//        $this->arrayInfo['telas'] = $p->getPermissao(); 
//        $this->loadTemplate("categorias_add", $this->arrayInfo);
//    }

    public function add() {
        if (isset($_POST['adicionarCategoria'])) {

            $categoria = $_POST['categoria'];
            $situacao = $_POST['situacao'];

            $categorias = new Categorias();
            if ($categorias->add($categoria, $situacao)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Categoria cadastrada com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Esta categoria já foi cadastrado no sistema !</div>';
            }
        }
        
        $p = new Permissao();
         
        $data['telas'] = $p->getPermissao(); 
        $this->loadTemplate('categorias_add', $data);
    }

    public function edit($id) {

        if (!empty($_POST['categoria'])) {
            $categoria = $_POST['categoria'];
            $situacao = isset($_POST['situacao']) ? 1 : 0;

            $categorias = new Categorias();
            if (empty($categorias->edit_action($id, $categoria, $situacao))) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Categoria editada com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Erro ao editar a categoria!</div>';
            }
        }
        $categorias = new Categorias();
        $p = new Permissao();
         
        $data['telas'] = $p->getPermissao(); 
        $data['categoria_info'] = $categorias->getInfo($id);
        $this->loadTemplate('categorias_edit', $data);
    }

    public function delete($id) {

        if (!$id) {
            echo '<script>alert("Falha ao excluir a categoria!");</script>';
        }

        $categorias = new Categorias();
        $categorias->delete($id);

        header("Location:" . BASE_URL . "categorias");
        exit;
    }

}
