<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Produtos;
use \Models\Fornecedores;
use \Models\Categorias;
use \Models\Home;
use \Models\Permissao;

class GraficosController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'graficos'
        );
    }

    public function index() {
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao(); 
        $this->loadTemplate('graficos', $this->arrayInfo);
    }

    public function grafico_produtos() {
        
        $home = new Home();
        //dados para o grafico de produtos adicionados no mês/ano
        $this->arrayInfo['total_mes'] = $home->getTotalDeProdutoMes();

        $this->loadView("grafico_produtos", $this->arrayInfo);
    }

}
