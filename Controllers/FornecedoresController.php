<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Fornecedores;
use \Models\Permissao;


class FornecedoresController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'fornecedores'
        );
    }

    public function index() {

        $fornecedores = new Fornecedores();
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao();  

        $this->arrayInfo['list'] = $fornecedores->getList();
        $this->loadTemplate('fornecedores', $this->arrayInfo);
    }

//    public function add() {
//        $p = new Permissao();
//         
//        $this->arrayInfo['telas'] = $p->getPermissao();
//        $this->loadTemplate("fornecedores_add", $this->arrayInfo);
//    }

    public function add() {

        if (isset($_POST['adicionarFornecedor'])) {

            $nome = $_POST['nome'];
            $razao = $_POST['razao'];
            $email = $_POST['email'];
            $telefone = $_POST['telefone'];
            $cnpj = $_POST['cnpj'];
            $cep = $_POST['cep'];
            $logradouro = $_POST['logradouro'];
            $numero = $_POST['numero'];
            $complemento = $_POST['complemento'];
            $bairro = $_POST['bairro'];
            $cidade = $_POST['cidade'];
            $estado = $_POST['estado'];
            $situacao = $_POST['situacao'];

            $fornecedores = new Fornecedores();
            if ($fornecedores->add($nome, $razao, $email, $telefone, $cnpj, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Fornecedor cadastrado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Este fornecedor já foi cadastrado no sistema !</div>';
            }
        }
        $p = new Permissao();
         
        $data['telas'] = $p->getPermissao();
        $this->loadTemplate('fornecedores_add', $data);
    }

    public function edit($id) {

        if (!empty($_POST['email'])) {
            $nome = $_POST['nome'];
            $razao = $_POST['razao'];
            $email = $_POST['email'];
            $telefone = $_POST['telefone'];
            $cnpj = $_POST['cnpj'];
            $cep = $_POST['cep'];
            $logradouro = $_POST['logradouro'];
            $numero = $_POST['numero'];
            $complemento = $_POST['complemento'];
            $bairro = $_POST['bairro'];
            $cidade = $_POST['cidade'];
            $estado = $_POST['estado'];
            $situacao = isset($_POST['situacao']) ? 1 : 0;

            $fornecedores = new Fornecedores();
            if (empty($fornecedores->edit_action($id, $nome, $razao, $email, $telefone, $cnpj, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao))) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Fornecedor editado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Erro ao editar o fornecedor!</div>';
            }
        } 
        $fornecedores = new Fornecedores();
        $p = new Permissao();
         
        $data['telas'] = $p->getPermissao();
        $data['info'] = $fornecedores->getInfo($id);
        $this->loadTemplate('fornecedores_edit', $data);
    }

    public function delete($id) {

        if (!$id) {
            echo '<script>alert("Falha ao excluir o fornecedor!");</script>';
        }

        $fornecedores = new Fornecedores();
        $fornecedores->delete($id);

        header("Location:" . BASE_URL . "fornecedores");
        exit;
    }

}
