<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Fornecedores;
use \Models\Compras;
use \Models\Produtos;
use Models\Categorias;

class ComprasController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'compras'
        );
    }

    public function index() {

        $c = new Compras();
        $data['statuses'] = array(
            '0' => 'Aguardando Pgto.',
            '1' => 'Pago',
            '2' => 'Cancelado'
        );
        $offset = 0;

        $data['compra_lista'] = $c->getList($offset);
        $this->loadTemplate('compras', $data);
    }

    public function add() {

        if (!isset($_SESSION['StockPower']['name']) || empty($_SESSION['StockPower']['name'])) {
            header('Location:' . BASE_URL . "/login");
        }

        $usuario_id = $_SESSION['StockPower']['id'];
        $c = new Compras();
        $f = new Fornecedores();

        $fornecedores = $f->getFornecedores();

        if (isset($_POST['fornecedor']) && !empty($_POST['fornecedor'])) {
            
            $fornecedor_id = $_POST['fornecedor'];
            $situacao = $_POST['situacao'];
            $quantidade = $_POST['quant'];

            $c->addCompra($fornecedor_id, $usuario_id, $quantidade, $situacao);
            header("Location: " . BASE_URL . "compras");
        }
        $data = array(
            'titulo' => 'Nova compra',
            'fornecedores' => $fornecedores
        );

        $this->loadTemplate('compras_add', $data);
    }

}
