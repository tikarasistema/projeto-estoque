<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Clientes;
use \Models\Vendas;
use \Models\Produtos;
use Models\Categorias;
use \Models\Permissao;

class VendasController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'vendas'
        );
    }

    public function index() {

        $v = new vendas();
        $data['statuses'] = array(
            '0' => 'Aguardando Pgto.',
            '1' => 'Pago',
            '2' => 'Cancelado'
        );
        
        $p = new Permissao();
         
        $data['telas'] = $p->getPermissao();  
        $data['venda_lista'] = $v->getList();
        $this->loadTemplate('vendas', $data);
    }

    //detalhes dos dados do cliente na venda
    public function venda_detalhes($id) {
        $v = new Vendas();
        $this->arrayInfo['detalhes'] = $v->getDetalheById($id);
        $this->arrayInfo['produtoItemVenda'] = $v->getFindProdutoVenda($id);

        $this->loadTemplate('venda_detalhes', $this->arrayInfo);
    }

    public function add() {

        if (!isset($_SESSION['StockPower']['name']) || empty($_SESSION['StockPower']['name'])) {
            header('Location:' . BASE_URL . "/login");
        }

        $usuario_id = $_SESSION['StockPower']['id'];
        $v = new Vendas();
        $c = new Clientes();

        $clientes = $c->getClientes();
        $data['clientes'] = $clientes;

        if (isset($_POST['cliente']) && !empty($_POST['cliente'])) {
            $cliente_id = $_POST['cliente'];
            $situacao = $_POST['situacao'];
            $desconto = $_POST['desconto'];
            $quantidade = isset($_POST['quant']) ? $_POST['quant'] : false;

            if ($v->addVenda($cliente_id, $usuario_id, $quantidade, $situacao, $desconto)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Venda cadastrada com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Você tem uma venda aberta para este cliente !</div>';
            }
        }
        
        $p = new Permissao(); 
        $data['telas'] = $p->getPermissao();
        $this->loadTemplate('vendas_add', $data);
    }

    public function edit($id) {

        $data['statuses'] = array(
            '0' => 'Aguardando Pgto.',
            '1' => 'Pago',
            '2' => 'Cancelado'
        );

        if (!isset($_SESSION['StockPower']['name']) || empty($_SESSION['StockPower']['name'])) {
            header('Location:' . BASE_URL . "/login");
        }

        $usuario_id = $_SESSION['StockPower']['id'];
        $v = new Vendas();
        $c = new Clientes();

        $clientes = $c->getClientes();
        $data['clientes'] = $clientes;

        if (isset($_POST['cliente']) && !empty($_POST['cliente'])) {

            $cliente_id = $_POST['cliente'];
            $situacao = $_POST['situacao'];
            $desconto = $_POST['desconto'];
            //$quantidade = $_POST['quant'];
            $quant = isset($_POST['quant']) ? $_POST['quant'] : false;
                       
            if ($v->editVenda($id, $cliente_id, $usuario_id, $quant, $situacao, $desconto)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Venda editada com sucesso!</div>';
                header('Location:' . BASE_URL . "vendas");
                exit;
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Não foi possíve editar uma venda !</div>';
                header('Location:' . BASE_URL . "vendas");
                exit;
            }
        }
        
        $p = new Permissao(); 
        $data['telas'] = $p->getPermissao(); 
        $data['venda'] = $v->getInfo($id);
        $this->loadTemplate('venda_editar', $data);
    }

    public function delete($id) {
        $data['statuses'] = array(
            '0' => 'Aguardando Pgto.',
            '1' => 'Pago',
            '2' => 'Cancelado'
        );

        $c = new Clientes();
        $clientes = $c->getClientes();
        $data['clientes'] = $clientes;

        if (!$id) {
            $data['msg'] = '<div class="alert alert-danger" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Falha ao apagar item da venda  !</div>';
        }

        $v = new Vendas();
        $v->delete($id);

        $data['venda'] = $v->getInfo($id);
        $this->loadTemplate('venda_editar', $data);
    }
}
