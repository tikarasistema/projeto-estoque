<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Permissao;

class PermissaoController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'permissao'
        );
    }

    public function index() {

        $p = new Permissao();
         
        //$this->arrayInfo['telas'] = $p->getPermissao(); 
        $this->arrayInfo['list'] = $p->getList();
        $this->loadTemplate('permissao', $this->arrayInfo);
    }

    public function add() {
        //$p = new Permissao();
         
        //$this->arrayInfo['telas'] = $p->getPermissao();
        $this->loadTemplate("permissao_add", $this->arrayInfo);
    }

    public function add_action() {

        if (isset($_POST['adicionarPermissao'])) {
            $descricao = $_POST['descricao'];
            $situacao = $_POST['situacao'];
            if (isset($_POST['urls'])) {
                $telasPermissao = join(',', $_POST['urls']);
            }

            $p = new Permissao();
            if ($p->add($descricao, $telasPermissao, $situacao)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Permissão cadastrada com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Esta permissão já foi cadastrada no sistema !</div>';
            }
        }
        $this->loadTemplate('permissao_add', $data);
    }
    
    public function edit($id) {

        if (!empty($_POST['descricao'])) {
            $descricao = $_POST['descricao'];
            $situacao = isset($_POST['situacao']) ? 1 : 0;
            if (isset($_POST['urls'])) {
                $telasPermissao = join(',', $_POST['urls']);
            }

            $permissao = new Permissao();
            if (empty($permissao->edit_action($id,$descricao, $telasPermissao, $situacao))) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Permissão editada com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Erro ao editar a permissão!</div>';
            }
        }
        $permissao = new Permissao();        
        
        //$data['telas'] = $permissao->getPermissao(); 
        $data['permissao_list'] = $permissao->getList();
        $data['permissao_info'] = $permissao->getInfo($id);
        $this->loadTemplate('permissao_edit', $data);
    }
}
