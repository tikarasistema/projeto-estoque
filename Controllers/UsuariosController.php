<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Permissao;

class UsuariosController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'usuarios'
        );
    }

    public function index() {

        $usuarios = new Users();
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao(); 

        $this->arrayInfo['list'] = $usuarios->getList();
        $this->loadTemplate('usuarios', $this->arrayInfo);
    }

//    public function add() {
//        $p = new Permissao();
//        
//        $this->arrayInfo['permissao'] = $p->getPermissaoId(); 
//        $this->arrayInfo['telas'] = $p->getPermissao(); 
//        $this->loadTemplate("usuarios_add", $this->arrayInfo);
//    }

    public function add() {

        if (isset($_POST['adicionarUsuario'])) {

            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $situacao = $_POST['situacao'];
            $nivel = $_POST['nivel'];

            $usuarios = new Users();
            if ($usuarios->add($name, $email, $password, $situacao, $nivel)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Usuário cadastrado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Este usuário já foi cadastrado no sistema !</div>';
            }
        }
        
        $p = new Permissao();
        
        $data['permissao'] = $p->getPermissaoId(); 
        $data['telas'] = $p->getPermissao(); 
        $this->loadTemplate('usuarios_add', $data);
    }

    public function edit($id) {

        if (!empty($_POST['email'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $situacao = isset($_POST['situacao']) ? 1 : 0;
            $nivel = $_POST['nivel'];

            $usuarios = new Users();
            if (empty($usuarios->edit_action($id, $name, $email, $password, $situacao, $nivel))) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Usuário editado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Erro ao editar o usuário!</div>';
            }
        }
        $usuarios = new Users();
        $p = new Permissao();
         $data['permissao'] = $p->getPermissaoId(); 
        $data['telas'] = $p->getPermissao(); 
        $data['info'] = $usuarios->getInfo($id);
        $this->loadTemplate('usuarios_edit', $data);
    }

    public function delete($id) {

        if (!$id) {
            echo '<script>alert("Falha ao excluir o usuario!");</script>';
        }

        $usuarios = new Users();
        $usuarios->delete($id);

        header("Location:" . BASE_URL . "usuarios");
        exit;
    }

}
