<?php

namespace Controllers;

use \Core\Controller;

use \Models\Categorias;
use \Models\Produtos;
use \Models\Users;
use \Models\Fornecedores;
use \Models\Clientes;
use \Models\Vendas;
use \Models\Permissao;


class RelatoriosController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'relatorios'
        );
    }

    public function index() {
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao(); 
        $this->loadTemplate('relatorios', $this->arrayInfo);
    }

    public function relatorio_categorias() {
       
        $situacao = $_POST['situacao'];
         $cat = new Categorias();            
        $this->arrayInfo['list'] = $cat->escolherSituacaoRelCat($situacao);

        $this->loadView("relatorio_categorias", $this->arrayInfo);
    }

    public function relatorio_produtos() {
        
        $situacao = $_POST['situacao'];
        $produtos = new Produtos();
        $this->arrayInfo['list'] = $produtos->escolherSituacaoRelProd($situacao);

        $this->loadView("relatorio_produtos", $this->arrayInfo);
    }

    public function relatorio_usuarios(){ 
            
        $situacao = $_POST['situacao']; 
        
        $usuarios = new Users();
        $this->arrayInfo['list'] = $usuarios->escolherSituacaoRelUsu($situacao);

        $this->loadView("relatorio_usuarios", $this->arrayInfo);
    }
    
   public function relatorio_fornecedores() {
        
        $situacao = $_POST['situacao']; 
        $fornecedores = new Fornecedores();
        $this->arrayInfo['list'] = $fornecedores->escolherSituacaoRelFor($situacao);

        $this->loadView("relatorio_fornecedores", $this->arrayInfo);
    }
    
   public function relatorio_clientes() {
        
        $situacao = $_POST['situacao']; 
        $clientes = new Clientes();
        $this->arrayInfo['list'] = $clientes->escolherSituacaoRelCli($situacao);

        $this->loadView("relatorio_clientes", $this->arrayInfo);
    }
    
   public function relatorio_vendas() {
    
        $dtInicio = $_POST['dtInicio']; 
        $dtFinal = $_POST['dtFinal'];
        $status_venda = $_POST['status_venda']; 
        $situacao = $_POST['situacao'];
        $vendas = new Vendas();
        $this->arrayInfo['list'] = $vendas->filtrarVendaPorData($dtInicio,$dtFinal,$status_venda,$situacao);

        $this->loadView("relatorio_vendas", $this->arrayInfo);
    }

}
