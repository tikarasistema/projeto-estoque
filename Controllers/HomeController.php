<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Home;
//use \Models\Fornecedores;


class HomeController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'home'
        );
    }

    public function index() {
        $home = new Home();
        //dashboard - lista s ultimos 5 produtos adicionados 
        $this->arrayInfo['list'] = $home->getListaDeProdutos();
        //dashboard - qtde de produtos
        $this->arrayInfo['qtde_produtos'] = $home->getQuantidadeDeProdutos();        
        //dashboard - qtde de fornecedores
        $this->arrayInfo['qtde_fornecedores'] = $home->getQuantidadeDeFornecedores();
        // dashboard - despesas com produtos
        $this->arrayInfo['total_preco'] = $home->getTotalPrecoDeProdutos();
        
        $this->arrayInfo['lista'] = $home->getListaDeVendas();
        
        $this->arrayInfo['qtVendaAberta'] = $home->getQtVendaAberta();
        
        $this->arrayInfo['qtVendaFechada'] = $home->getQtVendaFechada();
        
        $this->loadTemplate('home', $this->arrayInfo);
    }

}
