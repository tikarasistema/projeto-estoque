<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Produtos;
use \Models\Fornecedores;
use \Models\Categorias;
use \Models\Permissao;

class ProdutosController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'produtos'
        );
    }

    public function index() {

        $produtos = new Produtos();
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao(); 
        $this->arrayInfo['list'] = $produtos->getList();
        $this->loadTemplate('produtos', $this->arrayInfo);
    }

//    public function add() {
//
//        $fornecedor = new Fornecedores();
//        $p = new Permissao();
//         
//        $this->arrayInfo['telas'] = $p->getPermissao(); 
//        $this->arrayInfo['fornecedor_list'] = $fornecedor->getList();
//
//        $categoria = new Categorias();
//        $this->arrayInfo['categoria_list'] = $categoria->getList();
//
//        $this->loadTemplate("produtos_add", $this->arrayInfo);
//    }

    public function add() {

        //if (isset($_POST['adicionarProduto'])) {
        if (!empty($_POST['adicionarProduto'])) {
            $nome = $_POST['nome'];
            $preco = $_POST['preco'];
            $qtde = $_POST['qtde'];
            $qtde_min = $_POST['qtde_min'];
            $categoria = $_POST['categoria'];
            $fornecedor = $_POST['fornecedor'];
            $situacao = $_POST['situacao'];

            $preco = str_replace('.', '', $preco);
            $preco = str_replace(',', '.', $preco);

            if (!empty($_FILES['imagem'])) {
                $imagem = $_FILES['imagem'];
                $file_name = md5(time() . rand(0, 99)) . '.png';
                move_uploaded_file($imagem['tmp_name'], 'media/produtos/' . $file_name);
                $imagem = $file_name;
            } else {
                $imagem = "";
            }

            $produtos = new Produtos();
            if ($produtos->add($nome, $preco, $qtde, $qtde_min, $categoria, $fornecedor, $imagem, $situacao)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Produto cadastrado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Este produto já foi cadastrado no sistema !</div>';
            }
        }
        
        $fornecedor = new Fornecedores();
        $categoria = new Categorias();
        $p = new Permissao();
        
        $data['telas'] = $p->getPermissao(); 
        $data['fornecedor_list'] = $fornecedor->getList();
        $data['categoria_list'] = $categoria->getList();
        $this->loadTemplate('produtos_add', $data);
    }



    public function edit($id) {

        if (!empty($_POST['nome'])) {
            $nome = $_POST['nome'];
            $preco = $_POST['preco'];
            $qtde = $_POST['qtde'];
            $qtde_min = $_POST['qtde_min'];
            $categoria = $_POST['categoria'];
            $fornecedor = $_POST['fornecedor'];
            $situacao = isset($_POST['situacao']) ? 1 : 0;

            $preco = str_replace('.', '', $preco);
            $preco = str_replace(',', '.', $preco);

            $produtos = new Produtos();
            if (empty($produtos->edit_action($id, $nome, $preco, $qtde, $qtde_min, $categoria, $fornecedor, $situacao))) {
                     $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Produto editado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Erro ao editar o produto!</div>';
            }
        } 
        
        $p = new Permissao(); 
        $data['telas'] = $p->getPermissao();
        $produtos = new Produtos();
        $data['produto_info'] = $produtos->getInfo($id);
        $categoria = new Categorias();
        $data['categoria_list'] = $categoria->getList();
        $fornecedor = new Fornecedores();
        $data['fornecedor_list'] = $fornecedor->getList();
        $this->loadTemplate('produtos_edit', $data);
    }

    public function delete($id) {

        if (!$id) {
            echo '<script>alert("Falha ao excluir o produto!");</script>';
        }

        $produtos = new Produtos();
        $produtos->delete($id);

        header("Location:" . BASE_URL . "produtos");
        exit;
    }

}
