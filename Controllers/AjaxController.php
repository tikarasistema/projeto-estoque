<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Produtos;

class AjaxController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'ajax'
        );
    }

    public function index() { }
    
    // faz busca do produto no banco
    public function search_produtos() {
        $data = array();
        $p = new Produtos();

        if(isset($_GET['q']) && !empty($_GET['q'])) {
            $q = addslashes($_GET['q']);
            $data = $p->searchProdutosByName($q);
        }

        echo json_encode($data);
    }
}
