<?php

namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Clientes;
use \Models\Permissao;

class ClientesController extends Controller {

    private $user;
    private $arrayInfo;

    public function __construct() {
        $this->user = new Users();

        if (!$this->user->isLogged()) {
            header("Location: " . BASE_URL . "login");
            exit;
        }

        $this->arrayInfo = array(
            'user' => $this->user,
            'menuActive' => 'clientes'
        );
    }

    public function index() {
        
        $c = new Clientes();
        $p = new Permissao();
         
        $this->arrayInfo['telas'] = $p->getPermissao();  
        $this->arrayInfo['list'] = $c->getList();
        $this->loadTemplate('clientes', $this->arrayInfo);
    }

//    public function add() {
//
//        $p = new Permissao();
//         
//        $this->arrayInfo['telas'] = $p->getPermissao(); 
//        $this->loadTemplate("clientes_add", $this->arrayInfo);
//    }

    public function add() {

        if (isset($_POST['adicionarCliente'])) {

            $nome = $_POST['nome'];
            $sobrenome = $_POST['sobrenome'];
            $email = $_POST['email'];
            $telefone = $_POST['telefone'];
            $cpf = $_POST['cpf'];
            $cep = $_POST['cep'];
            $logradouro = $_POST['logradouro'];
            $numero = $_POST['numero'];
            $complemento = $_POST['complemento'];
            $bairro = $_POST['bairro'];
            $cidade = $_POST['cidade'];
            $estado = $_POST['estado'];
            $situacao = $_POST['situacao'];

            $c = new Clientes();
            if ($c->add($nome, $sobrenome, $email, $telefone, $cpf, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao)) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Cliente cadastrado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Este cliente já foi cadastrado no sistema !</div>';
            }
        }
        $p = new Permissao();
        $data['telas'] = $p->getPermissao();
        $this->loadTemplate('clientes_add', $data);
    }

    public function edit($id) {

        if (!empty($_POST['email'])) {
            $nome = $_POST['nome'];
            $sobrenome = $_POST['sobrenome'];
            $email = $_POST['email'];
            $telefone = $_POST['telefone'];
            $cpf = $_POST['cpf'];
            $cep = $_POST['cep'];
            $logradouro = $_POST['logradouro'];
            $numero = $_POST['numero'];
            $complemento = $_POST['complemento'];
            $bairro = $_POST['bairro'];
            $cidade = $_POST['cidade'];
            $estado = $_POST['estado'];
            $situacao = isset($_POST['situacao']) ? 1 : 0;

            $c = new Clientes();
            if (empty($c->edit_action($id, $nome, $sobrenome, $email, $telefone, $cpf, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao))) {
                $data['msg'] = '<div class="alert alert-success" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Cliente editado com sucesso!</div>';
            } else {
                $data['msg'] = '<div class="alert alert-warning" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                     Ops ! Erro ao editar o cliente!</div>';
            }
        }   
                
            $c = new Clientes();
            $p = new Permissao();
         
          $data['telas'] = $p->getPermissao(); 
          $data['info'] = $c->getInfo($id);
         $this->loadTemplate('clientes_edit', $data);
    }

    public function delete($id) {

        if (!$id) {
            echo '<script>alert("Falha ao excluir o registro!");</script>';
        }

        $c = new Clientes();
        $c->delete($id);

        header("Location:" . BASE_URL . "clientes");
        exit;
    }

}
