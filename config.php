<?php

require 'vendor/autoload.php';
require 'environment.php';

$config = array();
if (ENVIRONMENT == 'development') {
    // Offline - Localhost
    define("BASE_URL", "http://localhost/projeto-estoque/");
    $config['dbname'] = 'db_stockpower2';
    $config['host'] = 'localhost';
    $config['dbuser'] = 'root';
    $config['dbpass'] = '';
} else {
    // Caso esteja no Servidor Online
    define("BASE_URL", "https://www.seusite.com.br/");
    $config['dbname'] = '';
    $config['host'] = 'localhost';
    $config['dbuser'] = '';
    $config['dbpass'] = '';
}

/* Status da venda */
define('STATUS_VENDA_FECHADA', '0');
define('STATUS_VENDA_ABERTA', '1');
define('STATUS_VENDA_CANCELADA', '2');

/* Situação do pagamento na venda */
define('AGUARDANDO_PAGAMENTO', '0');
define('PAGO', '1');
define('CANCELADO', '2');

/* Situação */
define('SITUACAO_INATIVO', '0');
define('SITUACAO_ATIVO', '1');

/* Parametros usados na tela de permissão */
define('USUARIOADD', '/projeto-estoque/usuarios/add');
define('USUARIOEDIT', '/projeto-estoque/usuarios/edit');
define('USUARIO', '/projeto-estoque/usuarios');
define('CLIENTEADD', '/projeto-estoque/clientes/add');
define('CLIENTEEDIT', '/projeto-estoque/clientes/edit');
define('CLIENTE', '/projeto-estoque/clientes');
define('FORNECEDORADD', '/projeto-estoque/fornecedores/add');
define('FORNECEDOREDIT', '/projeto-estoque/fornecedores/edit');
define('FORNECEDOR', '/projeto-estoque/fornecedores');
define('CATEGORIAADD', '/projeto-estoque/categorias/add');
define('CATEGORIAEDIT', '/projeto-estoque/categorias/edit');
define('CATEGORIA', '/projeto-estoque/categorias');
define('PRODUTOADD', '/projeto-estoque/produtos/add');
define('PRODUTOEDIT', '/projeto-estoque/produtos/edit');
define('PRODUTO', '/projeto-estoque/produtos');
define('VENDAADD', '/projeto-estoque/vendas/add');
define('VENDAEDIT', '/projeto-estoque/vendas/edit');
define('VENDA', '/projeto-estoque/vendas');
define('RELATORIO', '/projeto-estoque/relatorios');
define('GRAFICO', '/projeto-estoque/graficos');


global $db;
try {
    $db = new PDO("mysql:dbname=" . $config['dbname'] . ";host=" . $config['host'], $config['dbuser'], $config['dbpass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "ERRO: " . $e->getMessage();
    exit;
}