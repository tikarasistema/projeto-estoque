<?php

namespace Models;

use \Core\Model;

class Categorias extends Model {

    public function getList() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM CATEGORIAS");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
        // relatorio de categorias- escolha por situacao
        public function escolherSituacaoRelCat($situacao) {
      
        $data = array();
        $sql = $this->db->prepare("select * from categorias where situacao = :situacao");
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
      
    public function getInfo($id) {
        $data = array();

        $sql = $this->db->prepare("select * from categorias where idcategoria = :idcategoria");
        $sql->bindValue(":idcategoria", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }

    public function add($categoria, $situacao) {
        if ($this->verificaCategoria($categoria) == false) {
            $sql = $this->db->prepare("Insert into categorias set categoria = :categoria, situacao = :situacao");
            $sql->bindValue(":categoria", $categoria);
            $sql->bindValue(":situacao", $situacao);
            $sql->execute();
            return true;
        } else {
            return false;
        }
    }

     //verifica se existe uma categoria já cadastrada na base de dados
    private function verificaCategoria($categoria) {

        $sql = $this->db->prepare("select idcategoria from categorias where categoria = :categoria");
        $sql->bindValue(":categoria", $categoria);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edit_action($id, $categoria, $situacao) {

        try {
            $sql = "UPDATE categorias SET categoria = :categoria, situacao = :situacao WHERE idcategoria = :idcategoria";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":idcategoria", $id);
            $sql->bindValue(":categoria", $categoria);
            $sql->bindValue(":situacao", $situacao);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

    public function delete($id) {
        try {
            $sql = $this->db->prepare("delete from categorias where idcategoria = :idcategoria");
            $sql->bindValue(":idcategoria", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

}
