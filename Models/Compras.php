<?php

namespace Models;

use \Core\Model;

date_default_timezone_set('America/Sao_Paulo');

class Compras extends Model {

    public function getList($offset) {
        $array = array();

        $sql = $this->db->prepare("
			SELECT c.id, c.data, c.total_preco, c.situacao, f.nome
			FROM compras c 
                        LEFT JOIN fornecedores f ON f.idfornecedor = c.fornecedor_id
			ORDER BY c.data
			LIMIT $offset, 10");
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }

    public function getInfo($id) {
        $array = array();

        $sql = $this->db->prepare("
		SELECT *,( select f.nome from fornecedores f where f.idfornecedor = c.fornecedor_id ) as fornecedor_nome
		FROM compras c
		WHERE id = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array['info'] = $sql->fetch();
        }

        $sql = $this->db->prepare("
		SELECT ci.quant,
		       ci.compra_preco,
		       p.nome
		       FROM compraitem ci
		       LEFT JOIN produtos p
		       ON p.idproduto = ci.produto_id
		       WHERE ci.compra_id = :compra_id");
        $sql->bindValue(":compra_id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array['products'] = $sql->fetchAll();
        }

        return $array;
    }

    public function changeStatus($situacao, $id) {

        $sql = $this->db->prepare("UPDATE compras SET situacao = :situacao WHERE id = :id");
        $sql->bindValue(":situacao", $situacao);
        $sql->bindValue(":id", $id);
        $sql->execute();
    }

    public function addCompra($fornecedor_id, $usuario_id, $quant, $situacao) {
        $p = new Produtos();

        $sql = $this->db->prepare("INSERT INTO compras SET fornecedor_id = :fornecedor_id, usuario_id = :usuario_id, data = NOW(), total_preco = :total_preco, situacao = :situacao");
        $sql->bindValue(":fornecedor_id", $fornecedor_id);
        $sql->bindValue(":usuario_id", $usuario_id);
        $sql->bindValue(":total_preco", '0');
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        $idcompra = $this->db->lastInsertId();

        $total_preco = 0;

        foreach ($quant as $id_prod => $quant_prod) {
            $sql = $this->db->prepare("SELECT preco FROM produtos WHERE idproduto = :idproduto");
            $sql->bindValue(":idproduto", $id_prod);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                
                $row = $sql->fetch();
                $preco = $row['preco'];

                $sql1 = $this->db->prepare("INSERT INTO compraitem SET compra_id = :compra_id,produto_id = :produto_id, quant = :quant, compra_preco = :compra_preco");
                $sql1->bindValue(":compra_id", $idcompra);
                $sql1->bindValue(":produto_id", $id_prod);
                $sql1->bindValue(":quant", $quant_prod);
                $sql1->bindValue(":compra_preco", $preco);
                $sql1->execute();

                //atualiza o estoque na tabela de produtos
                $p->atualizaEstoque($id_prod, $quant_prod, $usuario_id);

                $total_preco += $preco * $quant_prod;
            }
        }

        $sql = $this->db->prepare("UPDATE compras SET total_preco = :total_preco WHERE id = :id");
        $sql->bindValue(":total_preco", $total_preco);
        $sql->bindValue(":id", $idcompra);
        $sql->execute();
    }

}
