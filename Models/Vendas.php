<?php

namespace Models;

use \Core\Model;

date_default_timezone_set('America/Sao_Paulo');

class Vendas extends Model {
    
    
    public function filtrarVendaPorData($dtInicio,$dtFinal,$status_venda,$situacao) {

        $data = array();
        $sql = $this->db->prepare("select c.*, v.id, v.data, v.total_preco, v.totalfinal, v.desconto, v.vldesconto,v.status_venda, v.situacao from vendas v inner join clientes c on v.cliente_id = c.idcliente where v.data between '".$dtInicio."' and '".$dtFinal."' and status_venda = :status_venda and v.situacao = :situacao");
        $sql->bindValue(":status_venda", $status_venda);
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    public function getList() {
        $array = array();

        $sql = $this->db->prepare("select v.*, v.situacao as pagamento,v.status_venda as status, c.* from vendas v left join clientes c on v.cliente_id = c.idcliente");
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }

    public function getInfo($id) {
        $array = array();

        $sql = $this->db->prepare("
		SELECT *,( select c.nome from clientes c where c.idcliente = v.cliente_id ) as cliente_nome
		FROM vendas v
		WHERE id = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array['info'] = $sql->fetch();
        }

        $sql = $this->db->prepare("
		SELECT vi.quant,
                       vi.venda_id,
		       vi.venda_preco,
                       v.total_preco,
                       v.id,
                       v.desconto,
                       v.totalfinal,
		       p.nome,
                       vi.produto_id
		       FROM vendaitem vi
		       LEFT JOIN produtos p
		       ON p.idproduto = vi.produto_id
                       inner join vendas v on v.id = vi.venda_id
		       WHERE vi.venda_id = :venda_id");
        $sql->bindValue(":venda_id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array['item_produtos'] = $sql->fetchAll();
        }

        return $array;
    }

    //atualiza a situacao do pagamento e o status da venda
    public function changeStatus($situacao, $id) {

        // situacao do pagamento = 1(pago) --> atualiza o status da venda para 0(fechado)
        if ($situacao == PAGO) {
            $alteraStatusFechado = STATUS_VENDA_FECHADA;
            $sql = $this->db->prepare("UPDATE vendas SET situacao = :situacao, status_venda = :status_venda WHERE id = :id");
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":status_venda", $alteraStatusFechado);
            $sql->bindValue(":id", $id);
            $sql->execute();

            // situacao do pagamento = 2(cancelado) --> atualiza o status da venda para 0(fechado)
        } elseif ($situacao == CANCELADO) {
            $alteraStatusFechado = STATUS_VENDA_FECHADA;
            $sql = $this->db->prepare("UPDATE vendas SET situacao = :situacao, status_venda = :status_venda WHERE id = :id");
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":status_venda", $alteraStatusFechado);
            $sql->bindValue(":id", $id);
            $sql->execute();
        } else {
            die('<style>
.box-msg {position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);text-align: center;font-family: "Tahoma", sans-serif;}
.box-msg h2 {font-size: 86px;color: orange;margin: 0;padding: 0;margin-bottom: 20px;}
.box-msg p {font-size: 36px;font-weight: bold;margin: 0;padding: 0;}
</style>
<section class="content container-fluid"><div class="box-msg"><h2>Opsss...</h2><p>Altere o status do pagamento para finalizar esta venda!</p></div>
</section>');
        }
    }

    // dados do cliente para serem exibidos nos detalhes da venda
    public function getDetalheById($id) {
        $sql = $this->db->prepare("SELECT v.*, v.situacao as pagamento, c.* FROM vendas v inner join clientes c on v.cliente_id = c.idcliente where v.id = :id");
        $sql->bindParam(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $sql = $sql->fetch();
        }
        return $sql;
    }

    // busca os produtos vendidos naquela venda
    public function getFindProdutoVenda($id) {
        $sql = $this->db->prepare("Select vi.produto_id,p.nome,vi.venda_preco,vi.quant From vendaitem vi Inner Join produtos p On (p.idproduto = vi.produto_id) Where vi.venda_id = :id");
        $sql->bindParam(':id', $id);
        $result = $sql->execute();
        $rows = $sql->fetchAll();

        return $rows;
    }

    private function verificaVendaAberta($cliente_id, $status_venda) {

        $sql = $this->db->prepare("SELECT cliente_id, status_venda FROM vendas WHERE cliente_id = :cliente_id and status_venda = :status_venda");
        $sql->bindValue(":cliente_id", $cliente_id);
        $sql->bindValue(":status_venda", $status_venda);
        $sql->execute();
        if ($sql->rowCount() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addVenda($cliente_id, $usuario_id, $quant, $situacao, $desconto) {

        if ($quant > 0) {
            $p = new Produtos();
            //primeira venda salva com status em aberto(1)
            $status_venda = STATUS_VENDA_ABERTA;

            //verifica se existe uma venda aberta para o cliente
            if ($this->verificaVendaAberta($cliente_id, $status_venda)) {
                $sql = $this->db->prepare("INSERT INTO vendas SET cliente_id = :cliente_id, usuario_id = :usuario_id, data = NOW(), desconto = :desconto, total_preco = :total_preco, situacao = :situacao, status_venda = :status_venda");
                $sql->bindValue(":cliente_id", $cliente_id);
                $sql->bindValue(":usuario_id", $usuario_id);
                $sql->bindValue(":desconto", $desconto);
                $sql->bindValue(":total_preco", '0');
                $sql->bindValue(":situacao", $situacao);
                $sql->bindValue(":status_venda", $status_venda);
                $sql->execute();

                $idvenda = $this->db->lastInsertId();
                $total_preco = 0;

                foreach ($quant as $id_prod => $quant_prod) {
                    $sql = $this->db->prepare("SELECT preco FROM produtos WHERE idproduto = :idproduto");
                    $sql->bindValue(":idproduto", $id_prod);
                    $sql->execute();

                    if ($sql->rowCount() > 0) {

                        $row = $sql->fetch();
                        $preco = $row['preco'];

                        $sql1 = $this->db->prepare("INSERT INTO vendaitem SET venda_id = :venda_id, produto_id = :produto_id, quant = :quant, venda_preco = :venda_preco");
                        $sql1->bindValue(":venda_id", $idvenda);
                        $sql1->bindValue(":produto_id", $id_prod);
                        $sql1->bindValue(":quant", $quant_prod);
                        $sql1->bindValue(":venda_preco", $preco);
                        $sql1->execute();

                        //atualiza o estoque na tabela de produtos
                        $p->atualizaEstoque($id_prod, $quant_prod, $usuario_id);

                        $total_preco += $preco * $quant_prod;

                        //totalfinal é o total liquido da venda
                        $vldesconto = $total_preco * ($desconto / 100);
                        $totalfinal = $total_preco - $vldesconto;
                    }
                }

                $sql = $this->db->prepare("UPDATE vendas SET total_preco = :total_preco,vldesconto = :vldesconto, totalfinal = :totalfinal WHERE id = :id");
                $sql->bindValue(":total_preco", $total_preco);
                $sql->bindValue(":vldesconto", $vldesconto);
                $sql->bindValue(":totalfinal", $totalfinal);
                $sql->bindValue(":id", $idvenda);
                $sql->execute();

                //verifica se a situacao do pagamento está como Pago(1), se sim ele atualiza o status da venda para fechado(0)
                if ($situacao == PAGO) {

                    $alteraStatusFechado = STATUS_VENDA_FECHADA;

                    $sql = $this->db->prepare("UPDATE vendas SET status_venda = :status_venda WHERE id = :id");
                    $sql->bindValue(":status_venda", $alteraStatusFechado);
                    $sql->bindValue(":id", $idvenda);
                    $sql->execute();
                }

                return true;
            } else {
                return false;
            }
        } else {
            die('Ops ! Não é possível finalizar uma venda sem produto !');
            exit;
        }
    }

    public function editVenda($id, $cliente_id, $usuario_id, $quant, $situacao, $desconto) {

        if ($situacao == AGUARDANDO_PAGAMENTO) {

            if (!$quant) {
                
                $alteraStatusFechado = STATUS_VENDA_ABERTA;
                $sql = $this->db->prepare("UPDATE vendas SET situacao = :situacao, status_venda = :status_venda WHERE id = :id");
                $sql->bindValue(":situacao", $situacao);
                $sql->bindValue(":status_venda", $alteraStatusFechado);
                $sql->bindValue(":id", $id);
                $sql->execute();
                
            } else {
                
                $p = new Produtos();

                $sql = $this->db->prepare("update vendas SET cliente_id = :cliente_id, usuario_id = :usuario_id, desconto = :desconto, situacao = :situacao where id = :id ");
                $sql->bindValue(":cliente_id", $cliente_id);
                $sql->bindValue(":usuario_id", $usuario_id);
                $sql->bindValue(":desconto", $desconto);
                $sql->bindValue(":situacao", $situacao);
                $sql->bindValue(":id", $id);
                $sql->execute();

                $total_preco = 0;

                foreach ($quant as $id_prod => $quant_prod) {

                    $sql = $this->db->prepare("SELECT preco FROM produtos WHERE idproduto = :idproduto");
                    $sql->bindValue(":idproduto", $id_prod);
                    $sql->execute();

                    if ($sql->rowCount() > 0) {

                        $row = $sql->fetch();
                        $preco = $row['preco'];

                        //insere os itens da venda
                        $sql1 = $this->db->prepare("INSERT INTO vendaitem SET venda_id = :venda_id, produto_id = :produto_id, quant = :quant, venda_preco = :venda_preco");
                        $sql1->bindValue(":venda_id", $id);
                        $sql1->bindValue(":produto_id", $id_prod);
                        $sql1->bindValue(":quant", $quant_prod);
                        $sql1->bindValue(":venda_preco", $preco);
                        $sql1->execute();

                        //atualiza o estoque na tabela de produtos
                        $p->atualizaEstoque($id_prod, $quant_prod, $usuario_id);

                        $total_preco += $preco * $quant_prod;

                        //totalfinal é o total liquido da venda
                        $vldesconto = $total_preco * ($desconto / 100);
                        $totalfinal = $total_preco - $vldesconto;
                    }
                }

                //atualiza os valores na venda
                $sql2 = $this->db->prepare("UPDATE vendas SET total_preco = :total_preco,vldesconto = :vldesconto, totalfinal = :totalfinal WHERE id = :id");
                $sql2->bindValue(":total_preco", $total_preco);
                $sql2->bindValue(":vldesconto", $vldesconto);
                $sql2->bindValue(":totalfinal", $totalfinal);
                $sql2->bindValue(":id", $id);
                $sql2->execute();
            }

        } else if ($situacao == PAGO) {

            if ($this->changeStatus($situacao, $id)) {
                $alteraStatusFechado = STATUS_VENDA_FECHADA;
                $sql = $this->db->prepare("UPDATE vendas SET situacao = :situacao, status_venda = :status_venda WHERE id = :id");
                $sql->bindValue(":situacao", $situacao);
                $sql->bindValue(":status_venda", $alteraStatusFechado);
                $sql->bindValue(":id", $id);
                $sql->execute();
            }
        } else if ($situacao == CANCELADO) {

            if ($this->changeStatus($situacao, $id)) {
              
                $alteraStatusFechado = STATUS_VENDA_FECHADA;
                $sql = $this->db->prepare("UPDATE vendas SET situacao = :situacao, status_venda = :status_venda WHERE id = :id");
                $sql->bindValue(":situacao", $situacao);
                $sql->bindValue(":status_venda", $alteraStatusFechado);
                $sql->bindValue(":id", $id);
                $sql->execute();
            }
        } else {
            echo 'De ruim';
            exit;
        }
    }

    public function delete($id) {

        $sql = $this->db->prepare("delete from vendaitem where venda_id = :venda_id");
        $sql->bindValue(":venda_id", $id);
        $sql->execute();

        $total_preco = 0;
        $vldesconto = 0;
        $totalfinal = 0;
        $desconto = 0;

        $sql = $this->db->prepare("UPDATE vendas SET total_preco = :total_preco, vldesconto = :vldesconto, totalfinal = :totalfinal, desconto = :desconto WHERE id = :id");
        $sql->bindValue(":total_preco", $total_preco);
        $sql->bindValue(":vldesconto", $vldesconto);
        $sql->bindValue(":totalfinal", $totalfinal);
        $sql->bindValue(":desconto", $desconto);
        $sql->bindValue(":id", $id);
        $sql->execute();
    }

}
