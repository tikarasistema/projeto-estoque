<?php

namespace Models;

use \Core\Model;

class Home extends Model {

    //PRODUTOS
    public function getQuantidadeDeProdutos() {
        $data = 0;

        $sql = $this->db->query("SELECT idproduto FROM PRODUTOS");

        if ($sql->rowCount() > 0) {
            $data = $sql->rowCount();
        }
        return $data;
    }
    
    public function getTotalPrecoDeProdutos() {
        $data = 0;

        $sql = $this->db->query("SELECT SUM(preco) as total FROM PRODUTOS");
        $sql->execute();

        $number = $sql->fetch();
        $data = $number['total'];

        return $data;
    }

    public function getListaDeProdutos() {
        $data = array();
        $sql = $this->db->prepare("SELECT * FROM PRODUTOS ORDER BY idproduto DESC LIMIT 5");
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    public function getTotalDeProdutoMes() {
        $data = array();

        $sql = $this->db->prepare("SELECT extract(month from dt_criacao)as mes, extract(year from dt_criacao)as ano, COUNT(idproduto) as total, nome FROM produtos p where extract(year from dt_criacao) between 2018 and 2050 group by extract(month from dt_criacao) order by mes asc");
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    //FORNECEDORES
    public function getQuantidadeDeFornecedores() {
        $data = 0;

        $sql = $this->db->query("SELECT idfornecedor FROM FORNECEDORES");

        if ($sql->rowCount() > 0) {
            $data = $sql->rowCount();
        }
        return $data;
    }

    //VENDAS
    public function getListaDeVendas() {
        $data = array();
        $sql = $this->db->prepare("SELECT v.*,c.nome,v.total_preco FROM vendas v inner join clientes c on v.cliente_id = c.idcliente left join vendaitem vi on vi.id = v.id WHERE v.status_venda = 0 AND YEAR(data) = YEAR(now()) AND MONTH(data) = MONTH(now()) order by data desc limit 5");
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    public function getQtVendaAberta() {
        $data = array();
        $sql = $this->db->prepare("select count(status_venda) as qtVendaAberta from vendas where status_venda = 1");
        $sql->execute();

        $number = $sql->fetch();
        $data = $number['qtVendaAberta'];

        return $data;
    }

    public function getQtVendaFechada() {
        $data = array();
        $sql = $this->db->prepare("select count(status_venda) as qtVendaFechada from vendas where status_venda = 0");
        $sql->execute();

        $number = $sql->fetch();
        $data = $number['qtVendaFechada'];

        return $data;
    }

}
