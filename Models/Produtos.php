<?php

namespace Models;

use \Core\Model;

date_default_timezone_set('America/Sao_Paulo');

class Produtos extends Model {

    // faz a busca do produto no banco
    public function searchProdutosByName($nome) {
        $array = array();

        $sql = $this->db->prepare("SELECT nome as name, preco as price, idproduto as id FROM produtos WHERE nome LIKE :nome LIMIT 10");
        $sql->bindValue(':nome', '%' . $nome . '%');
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }

    public function getList() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM PRODUTOS");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    //usado na venda
    public function getProduto($id) {
        $sql = "SELECT * FROM produtos WHERE idproduto = ?";
        $sql = $this->db->prepare($sql);
        $sql->execute(array($id));
        $sql = $sql->fetch();
        return $sql;
    }

    //usado na venda
    public function getProdutos() {
        $sql = "SELECT * FROM produtos WHERE situacao = ?";
        $sql = $this->db->prepare($sql);
        $sql->execute(array(1));
        $sql = $sql->fetchAll();
        return $sql;
    }

    public function getLowQuantidadeProdutos() {
        $array = array();

        $sql = "SELECT * FROM PRODUTOS WHERE qtde < qtde_min";
        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }

    public function getInfo($id) {
        $data = array();

        $sql = $this->db->prepare("select * from produtos where idproduto = :idproduto");
        $sql->bindValue(":idproduto", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }
    
                // relatorio de clientes- escolha por situacao
    public function escolherSituacaoRelProd($situacao) {
      
        $data = array();
        $sql = $this->db->prepare("select * from produtos where situacao = :situacao");
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    public function add($nome, $preco, $qtde, $qtde_min, $categoria, $fornecedor, $imagem, $situacao) {

        $dt_criacao = date("Y:m:d h:i:s");

        if ($this->verificaProduto($nome)== false) {
            $sql = $this->db->prepare("Insert into produtos set nome = :nome, preco = :preco, qtde = :qtde,qtde_min = :qtde_min, categoria = :categoria, fornecedor = :fornecedor, imagem = :imagem, situacao = :situacao, dt_criacao = :dt_criacao");
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":preco", $preco);
            $sql->bindValue(":qtde", $qtde);
            $sql->bindValue(":qtde_min", $qtde_min);
            $sql->bindValue(":categoria", $categoria);
            $sql->bindValue(":fornecedor", $fornecedor);
            $sql->bindValue(":imagem", $imagem);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":dt_criacao", $dt_criacao);
            $sql->execute();
            return true;
        } else {
            return false;
        }
    }

    //verifica se existe produto já cadastrada na base de dados
    private function verificaProduto($nome) {

        $sql = $this->db->prepare("select idproduto from produtos where nome = :nome");
        $sql->bindValue(":nome", $nome);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // baixar e atualizar o estoque do produto
    public function atualizaEstoque($id_prod, $quant_prod, $usuario_id) {
       
        $sql = $this->db->prepare("UPDATE produtos SET qtde = qtde - $quant_prod WHERE idproduto = :idproduto");
        $sql->bindValue(":idproduto", $id_prod);
        $sql->execute();

        //$this->setHistoric($id_prod, $id_user, 'Baixa');
    }

    public function edit_action($id, $nome, $preco, $qtde, $qtde_min, $categoria, $fornecedor, $situacao) {

        try {
            $sql = "UPDATE produtos SET nome = :nome, preco = :preco, qtde = :qtde,qtde_min = :qtde_min, categoria = :categoria, fornecedor = :fornecedor, situacao = :situacao WHERE idproduto = :idproduto";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":idproduto", $id);
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":preco", $preco);
            $sql->bindValue(":qtde", $qtde);
            $sql->bindValue(":qtde_min", $qtde_min);
            $sql->bindValue(":categoria", $categoria);
            $sql->bindValue(":fornecedor", $fornecedor);
            //$sql->bindValue(":imagem", $imagem);
            $sql->bindValue(":situacao", $situacao);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

    public function delete($id) {
        try {
            $sql = $this->db->prepare("delete from produtos where idproduto = :idproduto");
            $sql->bindValue(":idproduto", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

}
