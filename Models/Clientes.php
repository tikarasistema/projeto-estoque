<?php

namespace Models;

use \Core\Model;
date_default_timezone_set('America/Sao_Paulo');

class Clientes extends Model {
    
        public function getCliente($id){
        $sql = "SELECT * FROM clientes WHERE idcliente = ?";
        $sql = $this->db->prepare($sql);
        $sql->execute(array($id));
        $sql = $sql->fetch();
        return $sql;
    }
        //usado na venda
        public function getClientes() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM CLIENTES Where situacao = 1");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    
    public function getList() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM CLIENTES");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
    public function getInfo($id) {
        $data = array();

        $sql = $this->db->prepare("select * from clientes where idcliente = :idcliente");
        $sql->bindValue(":idcliente", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }
 
            // relatorio de clientes- escolha por situacao
    public function escolherSituacaoRelCli($situacao) {
      
        $data = array();
        $sql = $this->db->prepare("select * from clientes where situacao = :situacao");
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
    public function add($nome, $sobrenome, $email, $telefone, $cpf, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao) {
            $dt_criacao = date("Y:m:d h:i:s");
        if ($this->verificaCPF($cpf) == false) {
            $sql = $this->db->prepare("Insert into clientes set nome = :nome, sobrenome = :sobrenome, email = :email,telefone = :telefone,cpf = :cpf, cep = :cep, logradouro = :logradouro,numero = :numero, complemento = :complemento,bairro = :bairro, cidade = :cidade, estado = :estado, situacao = :situacao, dt_criacao = :dt_criacao");
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":sobrenome", $sobrenome);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":telefone", $telefone);
            $sql->bindValue(":cpf", $cpf);
            $sql->bindValue(":cep", $cep);
            $sql->bindValue(":logradouro", $logradouro);
            $sql->bindValue(":numero", $numero);
            $sql->bindValue(":complemento", $complemento);
            $sql->bindValue(":bairro", $bairro);
            $sql->bindValue(":cidade", $cidade);
            $sql->bindValue(":estado", $estado);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":dt_criacao", $dt_criacao);
            $sql->execute();
            return true;
        } else {
            return false;
        }
    }

    //verifica se existe um cpf já cadastrado na base de dados
    private function verificaCPF($cpf) {

        $sql = $this->db->prepare("select idcliente from clientes where cpf = :cpf");
        $sql->bindValue(":cpf", $cpf);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edit_action($id, $nome,$sobrenome, $email, $telefone, $cpf, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao) {

        try {
            $sql = "UPDATE clientes set nome = :nome, sobrenome = :sobrenome, email = :email,telefone = :telefone,cpf = :cpf, cep = :cep, logradouro = :logradouro,numero = :numero, complemento = :complemento,bairro = :bairro, cidade = :cidade, estado = :estado, situacao = :situacao WHERE idcliente = :idcliente";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":sobrenome", $sobrenome);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":telefone", $telefone);
            $sql->bindValue(":cpf", $cpf);
            $sql->bindValue(":cep", $cep);
            $sql->bindValue(":logradouro", $logradouro);
            $sql->bindValue(":numero", $numero);
            $sql->bindValue(":complemento", $complemento);
            $sql->bindValue(":bairro", $bairro);
            $sql->bindValue(":cidade", $cidade);
            $sql->bindValue(":estado", $estado);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":idcliente", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

    public function delete($id) {
        try {
            $sql = $this->db->prepare("delete from clientes where idcliente = :idcliente");
            $sql->bindValue(":idcliente", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

}
