<?php

namespace Models;

use \Core\Model;

class Users extends Model {

    // Metodo para verificar se o Usuario está logado
    public function isLogged() {

        // Se a sessão NÃO estiver vazia e EXISTIR, retorne true.
        if (!empty($_SESSION['StockPower'])) {
            return true;
        }

        // Caso nao exista, retorne false;
        return false;
    }

    // Metodo para verificar o Login
    public function validateLogin($email, $password) {

        $sql = $this->db->prepare("SELECT * FROM users WHERE email = :email AND password = :password");
        $sql->bindValue(':email', $email);
        $sql->bindValue(':password', $password);
        //$sql->bindValue(':password', md5($password));
        $sql->execute();

        if ($sql->rowCount() > 0) {
            // Se o usuario existe, armazenamos as informações dele, e criamos uma sessao para ele
            $row = $sql->fetch();
            $_SESSION['StockPower'] = $row;

            // Retorna true (verdadeiro) caso encontre algum usuario.
            return true;
        }

        // Caso não seja encontrado, retorna false (falso)
        return false;
    }

    public function getDados($id) {
        $sql = "SELECT * FROM users WHERE id = ?";
        $sql = $this->db->prepare($sql);
        $sql->execute(array($id));
        $sql = $sql->fetch();
        return $sql;
    }

    public function getList() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM USERS");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
        // relatorio de categorias- escolha por situacao
        public function escolherSituacaoRelUsu($situacao) {
      
        $data = array();
        $sql = $this->db->prepare("select * from users where situacao = :situacao");
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }    

    public function getCountUsers() {
        $data = array();

        $sql = $this->db->query("SELECT COUNT(*) AS users FROM users");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }

    public function getInfo($id) {
        $data = array();

        $sql = $this->db->prepare("select * from users where id = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }

    public function add($name, $email, $password, $situacao,$nivel) {
        if ($this->verificaEmail($email) == false) {
            $sql = $this->db->prepare("Insert into users set name = :name, email = :email, password = :password, situacao = :situacao, nivel = :nivel");
            $sql->bindValue(":name", $name);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":password", $password);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":nivel", $nivel);
            $sql->execute();
            return true;
        } else {
            return false;
        }
    }

    //verifica se existe um email já cadastrado na base de dados
    private function verificaEmail($email) {

        $sql = $this->db->prepare("select id from users where email = :email");
        $sql->bindValue(":email", $email);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edit_action($id, $name, $email, $password, $situacao,$nivel) {

        try {
            $sql = "UPDATE users SET name = :name, email = :email, password = :password, situacao = :situacao, nivel = :nivel WHERE id = :id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":name", $name);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":password", $password);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":nivel", $nivel);
            $sql->bindValue(":id", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

    public function delete($id) {
        try {
            $sql = $this->db->prepare("delete from users where id = :id");
            $sql->bindValue(":id", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

}

// End of Model