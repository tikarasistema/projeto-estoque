<?php

namespace Models;

use \Core\Model;

date_default_timezone_set('America/Sao_Paulo');

class Fornecedores extends Model {

    public function getList() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM FORNECEDORES");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
     public function getFornecedor($id){
        $sql = "SELECT * FROM fornecedores WHERE idfornecedor = ?";
        $sql = $this->db->prepare($sql);
        $sql->execute(array($id));
        $sql = $sql->fetch();
        return $sql;
    }
    
    public function getFornecedores() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM FORNECEDORES Where situacao = 1");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
        // relatorio de categorias- escolha por situacao
        public function escolherSituacaoRelFor($situacao) {
      
        $data = array();
        $sql = $this->db->prepare("select * from fornecedores where situacao = :situacao");
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    public function getInfo($id) {
        $data = array();

        $sql = $this->db->prepare("select * from fornecedores where idfornecedor = :idfornecedor");
        $sql->bindValue(":idfornecedor", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }

    public function add($nome, $razao, $email, $telefone, $cnpj, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao) {
        $dt_criacao = date("Y:m:d h:i:s");
        if ($this->verificaCNPJ($cnpj) == false) {
            $sql = $this->db->prepare("Insert into fornecedores set nome = :nome, razao = :razao, email = :email,telefone = :telefone,cnpj = :cnpj, cep = :cep, logradouro = :logradouro,numero = :numero, complemento = :complemento,bairro = :bairro, cidade = :cidade, estado = :estado, situacao = :situacao, dt_criacao = :dt_criacao");
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":razao", $razao);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":telefone", $telefone);
            $sql->bindValue(":cnpj", $cnpj);
            $sql->bindValue(":cep", $cep);
            $sql->bindValue(":logradouro", $logradouro);
            $sql->bindValue(":numero", $numero);
            $sql->bindValue(":complemento", $complemento);
            $sql->bindValue(":bairro", $bairro);
            $sql->bindValue(":cidade", $cidade);
            $sql->bindValue(":estado", $estado);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":dt_criacao", $dt_criacao);
            $sql->execute();
            return true;
        } else {
            return false;
        }
    }

    //verifica se existe um cnpj já cadastrado na base de dados
    private function verificaCNPJ($cnpj) {

        $sql = $this->db->prepare("select idfornecedor from fornecedores where cnpj = :cnpj");
        $sql->bindValue(":cnpj", $cnpj);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edit_action($id, $nome, $razao, $email, $telefone, $cnpj, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $situacao) {

        try {
            $sql = "UPDATE fornecedores SET nome = :nome, razao = :razao, email = :email, telefone = :telefone, cnpj = :cnpj, cep = :cep, logradouro = :logradouro,numero = :numero, complemento = :complemento, bairro = :bairro, cidade = :cidade, estado = :estado, situacao = :situacao WHERE idfornecedor = :idfornecedor";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":nome", $nome);
            $sql->bindValue(":razao", $razao);
            $sql->bindValue(":email", $email);
            $sql->bindValue(":telefone", $telefone);
            $sql->bindValue(":cnpj", $cnpj);
            $sql->bindValue(":cep", $cep);
            $sql->bindValue(":logradouro", $logradouro);
            $sql->bindValue(":numero", $numero);
            $sql->bindValue(":complemento", $complemento);
            $sql->bindValue(":bairro", $bairro);
            $sql->bindValue(":cidade", $cidade);
            $sql->bindValue(":estado", $estado);
            $sql->bindValue(":situacao", $situacao);
            $sql->bindValue(":idfornecedor", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

    public function delete($id) {
        try {
            $sql = $this->db->prepare("delete from fornecedores where idfornecedor = :idfornecedor");
            $sql->bindValue(":idfornecedor", $id);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

}
