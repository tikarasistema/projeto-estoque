<?php

namespace Models;

use \Core\Model;

class Permissao extends Model {

    public function getList() {
        $data = array();

        $sql = $this->db->query("SELECT * FROM PERMISSAO");

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    public function getPermissao() {

        $id = $_SESSION['StockPower']['nivel'];
        $data = array();

        $sql = $this->db->prepare("select telas from permissao where id_permissao = :id_permissao");
        $sql->bindValue(":id_permissao", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }

    //pega e mostra o nivel de permissao no usuarios
    public function getPermissaoId() {

        $situacao = SITUACAO_ATIVO;
        $data = array();

        $sql = $this->db->prepare("select id_permissao, descricao, situacao from permissao where situacao = :situacao");
        $sql->bindValue(":situacao", $situacao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetchAll();
        }
        return $data;
    }
    
   
    public function getInfo($id) {
        $data = array();

        $sql = $this->db->prepare("select * from permissao where id_permissao= :id_permissao");
        $sql->bindValue(":id_permissao", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
        }
        return $data;
    }
    
    private function verificaPermissao($descricao) {

        $sql = $this->db->prepare("select id_permissao from permissao where descricao = :descricao");
        $sql->bindValue(":descricao", $descricao);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add($descricao, $telasPermissao, $situacao) {
        if ($this->verificaPermissao($descricao) == false) {
        $usuario_id = $_SESSION['StockPower']['id'];
        $sql = $this->db->prepare("Insert into permissao set usuario_id = :usuario_id,descricao = :descricao,telas = :telas, situacao = :situacao");
        $sql->bindValue(":descricao", $descricao);
        $sql->bindValue(":telas", $telasPermissao);
        $sql->bindValue(":situacao", $situacao);
        $sql->bindValue(":usuario_id", $usuario_id);
        $sql->execute();
        return true;
        } else {
            return false;
        }
    }
    
    public function edit_action($id,$descricao, $telasPermissao, $situacao) {

        try {
            $sql = "UPDATE permissao SET descricao = :descricao,telas = :telas, situacao = :situacao WHERE id_permissao = :id_permissao";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id_permissao", $id);
            $sql->bindValue(":descricao", $descricao);
            $sql->bindValue(":telas", $telasPermissao);
            $sql->bindValue(":situacao", $situacao);
            $sql->execute();
        } catch (PDOException $ex) {
            echo "Error: " . $ex->getMessage();
        }
    }

}
