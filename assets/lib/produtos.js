$(document).ready(function () {
    $('input[name=preco]').mask("000,000,000,000,000,00", {reverse:true, placeholder:'R$ 0,00'});
});

$(document).ready(function () {
    $("form[name='registration']").validate({
        rules: {
            nome: "required",
            preco: "required",
            qtde: "required",
            qtde_min: "required",
            categoria : "required",
            fornecedor: "required",
            imagem: "required",
             nome: {
                required: true,
                minlength: 3
            },          
        },
        messages: {
            nome: "Informe o nome",
            preco: "Informe o preço",
            qtde: "Informe a quantidade",
            qtde_min: "Informe a quantidade minima",
            categoria: "Informe a categoria",
            fornecedor: "Informe o fornecedor",
            nome: {
                required: "Informe o produto",
                minlength: "O campo informado tem menos de 3 caracteres"
            },            
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});

$(document).ready(function () {
    $('.imagem-foto').hide();

    $('.buttonImg').bind('click', function(){
        $('.imagem-foto').toggle();
    });
});

    $(document).ready(function () {
        $('#example').DataTable({
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });
    });
  $(document).on("click", ".open-excluirRegistro", function () {
    var myBookId = $(this).data('delete-registro');
    $('#delete-modal #acaoRemoverRegistro').attr("href", "produtos/delete/"+myBookId);
});  
