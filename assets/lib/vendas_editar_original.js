function updateSubtotal(obj) {
	var quant = $(obj).val();
	if(quant <= 0) {
		$(obj).val(1);
		quant = 1;
	}

	var price = $(obj).attr('data-price');
	var subtotal = price * quant;

	$(obj).closest('tr').find('.subtotal').html('R$ '+Number(subtotal).toFixed(2));

	updateTotal();

}

function updateTotal() {
	var total = 0;

	for(var q=0;q<$('.p_quant').length;q++) {
		var quant = $('.p_quant').eq(q);

		var price = quant.attr('data-price');
		var subtotal = price * parseInt(quant.val());

		total += subtotal;
	}

	$('input[name=total_price]').val(Number(total).toFixed(2));
}

function excluirProd(obj) {
	$(obj).closest('tr').remove();
}

//funcao para adição de produto
function addProd(obj) {
	$('#add_prod').val('');
	var id = $(obj).attr('data-id');
	var name = $(obj).attr('data-name');
	var price = $(obj).attr('data-price');
       
	$('.searchresults').hide();

	if( $('input[name="quant['+id+']"]').length == 0 ) {
		var tr = 
		'<tr>'+
                '<td class="col-sm-8 col-md-1">'+id+'</td>'+
		'<td class="col-sm-8 col-md-6">'+name+'</td>'+
		'<td class="col-sm-1 col-md-1 text-center">'+
		'<input type="number" name="quant['+id+']" class="p_quant" value="1" onchange="updateSubtotal(this)" data-price="'+price+'" />'+
		'</td>'+
		'<td class="col-sm-1 col-md-1 text-center">R$ '+Number(price).toFixed(2)+'</td>'+
		'<td class="col-sm-1 col-md-1 text-center subtotal">R$ '+Number(price).toFixed(2)+'</td>'+
		'<td class="col-sm-1 col-md-1"><a href="javascript:;" onclick="excluirProd(this)">Remover</a></td>'+
		'</tr>';

		$('#produtos_table').append(tr);
	}

	updateTotal();

}

$(function(){

	$('input[name=total_price]').mask('000.000.000.000.000,00', {reverse:true, placeholder:"0,00"});
        
	$('#add_prod').on('keyup', function(){
		var datatype = $(this).attr('data-type');
		var q = $(this).val();

		if(datatype != '') {
			$.ajax({
				url:'/projeto-estoque/Ajax/'+datatype,
				type:'GET',
				data:{q:q},
				dataType:'json',
				success:function(json) {
					if( $('.searchresults').length == 0 ) {
					$('#add_prod').after('<div class="searchresults"></div>');
					}
					//$('.searchresults').css('left', $('#add_prod').offset().left+'px');
					//$('.searchresults').css('top', $('#add_prod').offset().top+$('#add_prod').height()+3+'px');

					var html = '';

					for(var i in json) {
                                         html += '<div class="si"><table class="table table-hover table-sm"><thead><tr><th>ID</th><th>Nome do Produto</th><th>Preço</th><th>Ação</th></tr></thead><tbody><tr><td>'+json[i].id+'</td><td>'+json[i].name+'</td><td>'+json[i].price+'</td><td><a href="javascript:;" onclick="addProd(this)" data-id="'+json[i].id+'" data-price="'+json[i].price+'" data-name="'+json[i].name+'">Adicionar</a></td></tr></tbody></table></div>';
					}

					$('.searchresults').html(html);
					$('.searchresults').show();
				}
			});
		}

	});

});