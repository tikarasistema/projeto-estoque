function updateSubtotal(obj) {
    var quant = $(obj).val();
    if (quant <= 0) {
        $(obj).val(1);
        quant = 1;
    }

    var price = $(obj).attr('data-price');
    var subtotal = price * quant;

    $(obj).closest('tr').find('.subtotal').html('R$ ' + Number(subtotal).toFixed(2));

    updateTotal();

}

function updateTotal() {
    var total = 0;
    var vldesconto = 0;
    var totalfinal = 0;
    var desconto = $('input[name=desconto]').val();

    for (var q = 0; q < $('.p_quant').length; q++) {
        var quant = $('.p_quant').eq(q);

        var price = quant.attr('data-price');
        var subtotal = price * parseInt(quant.val());

        total += subtotal;
        vldesconto = total * (desconto / 100);
        totalfinal = total - vldesconto;
    }

    $('input[name=total_price]').val(Number(total).toFixed(2));
    $('input[name=desconto]').val(desconto);
    $('input[name=vldesconto]').val(Number(vldesconto).toFixed(2));
    $('input[name=totalfinal]').val(Number(totalfinal).toFixed(2));
}

function excluirProd(obj) {
    $(obj).closest('tr').remove();
}

//funcao para adição de produto
function addProd(obj) {
    $('#add_prod').val('');
    var id = $(obj).attr('data-id');
    var name = $(obj).attr('data-name');
    var price = $(obj).attr('data-price');

    $('.searchresults').hide();

    if ($('input[name="quant[' + id + ']"]').length == 0) {
        var tr =
                '<tr>' +
                '<td>' + id + '</td>' +
                '<td>' + name + '</td>' +
                '<td>' +
                '<input type="number" name="quant[' + id + ']" class="p_quant" value="1" onchange="updateSubtotal(this)" data-price="' + price + '" />' +
                '</td>' +
                '<td>' + Number(price).toFixed(2) + '</td>' +
                '<td class="subtotal">' + Number(price).toFixed(2) + '</td>' +
                '<td><a href="javascript:;" onclick="excluirProd(this)">Remover</a></td>' +
                '</tr>';
        $('#produtos_table').append(tr);
    }

    updateTotal();

}

$(function () {

    $('input[name=total_price]').mask('000.000.000.000.000,00', {reverse: true, placeholder: "0,00"});
    $('input[name=desconto]').mask('000', {reverse: true, placeholder: "0"});
    $('input[name=vldesconto]').mask('000.000.000.000.000,00', {reverse: true, placeholder: "0,00"});
    $('input[name=totalfinal]').mask('000.000.000.000.000,00', {reverse: true, placeholder: "0,00"});

    $('#add_prod').on('keyup', function () {
        var datatype = $(this).attr('data-type');
        var q = $(this).val();

        if (datatype != '') {
            $.ajax({
                url: '/projeto-estoque/Ajax/' + datatype,
                type: 'GET',
                data: {q: q},
                dataType: 'json',
                success: function (json) {
                    if ($('.searchresults').length == 0) {
                        $('#add_prod').after('<div class="searchresults"></div>');
                    }

                    var html = '';

                    for (var i in json) {
                        html += '<div class="box-body">' +
                                '<table class="table table-hover">' +
                                '<tr>' +
                                '<th>ID</th>' +
                                '<th>Nome do Produto</th>' +
                                '<th>Preço Unit.</th>' +
                                '<th>Ação</th>' +
                                '</tr>' +
                                '<tr>' +
                                '<td>' + json[i].id + '</td>' +
                                '<td class="col-md-6">' + json[i].name + '</td>' +
                                '<td>' + json[i].price + '</td>' +
                                '<td><a href="javascript:;" onclick="addProd(this)" data-id="' + json[i].id + '" data-price="' + json[i].price + '" data-name="' + json[i].name + '">Adicionar</a></td>' +
                                '</tr>' +
                                '</table>' +
                                '</div>';
                    }

                    $('.searchresults').html(html);
                    $('.searchresults').show();
                }
            });
        }

    });

});