  $(document).ready(function () {
    $("form[name='registration']").validate({
        rules: {
            name: "required",
            email: "required",
            password: "required",
             name: {
                required: true,
                minlength: 3
            },
             password: {
                required: true,
                minlength: 3
            },
            email: {
                required: true,
                email: true
            }
           
        },
        messages: {
            name: {
                required: "Informe o nome ",
                minlength: "O campo informado tem menos de 3 caracteres"
            },
            password: {
                required: "Informe a senha",
                minlength: "O campo informado tem menos de 3 caracteres"
            },
            
            email: {
                required: "Informe o e-mail",
                email: "Informe um e-mail válido"
            },
            
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});  
    
    
    $(document).ready(function () {
        $('#example').DataTable({
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });
    });
  $(document).on("click", ".open-excluirRegistro", function () {
    var myBookId = $(this).data('delete-registro');
    $('#delete-modal #acaoRemoverRegistro').attr("href", "usuarios/delete/"+myBookId);
});  