<?php

$telasPermissao = '';
if (isset($_POST['urls'])) {
    $telasPermissao = join(',', $_POST['urls']);
}

//constantes sendo chamadas do parametro no config.php
$telas = array(
    'usuario' => array(
        'titulo' => 'Usuários',
        'telas' => array(
            USUARIOADD => 'Cadastrar',
            USUARIOEDIT => 'Editar',
            USUARIO => 'Listar'
        )
    ),
    'cliente' => array(
        'titulo' => 'Clientes',
        'telas' => array(
            CLIENTEADD => 'Cadastrar',
            CLIENTEEDIT => 'Editar',
            CLIENTE => 'Listar'
        )
    ),
    'fornecedor' => array(
        'titulo' => 'Fornecedores',
        'telas' => array(
            FORNECEDORADD => 'Cadastrar',
            FORNECEDOREDIT => 'Editar',
            FORNECEDOR => 'Listar'
        )
    ),
    'categoria' => array(
        'titulo' => 'Categorias',
        'telas' => array(
            CATEGORIAADD => 'Cadastrar',
            CATEGORIAEDIT => 'Editar',
            CATEGORIA => 'Listar'
        )
    ),
    'produto' => array(
        'titulo' => 'Produtos',
        'telas' => array(
            PRODUTOADD => 'Cadastrar',
            PRODUTOEDIT => 'Editar',
            PRODUTO => 'Listar'
        )
    ),
    'venda' => array(
        'titulo' => 'Vendas',
        'telas' => array(
            VENDAADD => 'Cadastrar',
            VENDAEDIT => 'Editar',
            VENDA => 'Listar'
        )
    ),
    'relatorio' => array(
        'titulo' => 'Relatórios',
        'telas' => array(
            RELATORIO => 'Listar'
        )
    ),
    'grafico' => array(
        'titulo' => 'Gráficos',
        'telas' => array(
            GRAFICO => 'Listar'
        )
    )
);


;
?>
